#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import sys
import xml.etree.ElementTree as etree

# https://matplotlib.org/devdocs/gallery/misc/table_demo.html#sphx-glr-gallery-misc-table-demo-py

filename = "jobs.xml"
if len(sys.argv) > 1:
    filename = sys.argv[1]

tree = etree.parse(filename)
roots = tree.findall('JobPool')
if len(roots) == 0:
    print("Found no 'JobPool' root")
else:
    if len(roots) > 1:
        print("Found more than one 'JobPool' root")
    root = roots[0]

ID_NAME = "id"
ASSIGN_TIME_NAME = "assign_time"
COMPLETE_TIME_NAME = "complete_time"
RANKS_NAME = "ranks"

plt.figure()

cmap = plt.get_cmap('hsv')
NUM_COLORS = 20

count = 0
for job in root:
    for child in job:
        if child.tag == ASSIGN_TIME_NAME:
            start_time = int(child.text)
        elif child.tag == COMPLETE_TIME_NAME:
            end_time = int(child.text)
        elif child.tag == RANKS_NAME:
            ranks = []
            old_r = None
            for r in child.text.split(" ")[:-1]:
                r = float(r)
                if not old_r is None:
                    ranks.append((old_r + r) / 2.)
                ranks.append(r)
                old_r = r

            # ranks = [int(r) for r in child.text.split(" ")[:-1]]
        elif child.tag == ID_NAME:
            jid = int(child.text)

    # print("Found job {} assigned to ranks '{}'".format(jid, ranks))
    print("Found job {} assigned to ranks '{}' start: {}, end: {}".format(jid, ranks, start_time, end_time))

    y_offset = start_time
    bar_width = 0.5
    plt.bar(ranks, end_time - start_time, bar_width, bottom=y_offset, color=cmap((float(jid) / NUM_COLORS) % 1))
    # cell_text.append(['%1.1f' % (x / 1000.0) for x in y_offset])

    count += 1

print("Found {} jobs...".format(count))

plt.show()