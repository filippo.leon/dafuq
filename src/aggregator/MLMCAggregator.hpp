/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 13/12/17.
//

#include "user/IAggregate.hpp"

#include "OnePassAggregate.hpp"

namespace dafuq::base {

template<class Interpolator, class Aggregate_ = user::IAggregate, class KeyType_ = int>
class MLMCAggregator {
public:
    using Aggregate = Aggregate_;
    using KeyType = KeyType_;

    template<class T>
    struct lowHighPair : public std::pair<OnePassAggregate<T>, OnePassAggregate<T>> {
        auto &low() { return this->first; }

        auto &high() { return this->second; }
    };

    static_assert(!std::is_integral_v < Aggregate > ,
                  "Aggregate cannot be of integral type, use floating point instead.");

    MLMCAggregator(const std::tuple<Interpolator> &interpolator)
            : interpolator(std::get<0>(interpolator)) {}

    MLMCAggregator(const Interpolator &interpolator) : interpolator(interpolator) {}

    template<class AggregateInfo>
    void add(const std::pair <KeyType, Aggregate> &aggr, const AggregateInfo &info) {
        int level = info.level;
        IntraLevelType ilt = info.intra_level_type;

        int key = aggr.first;

        reserve(level, key);

        switch (ilt) {
            case IntraLevelType::None:
                debug::log_error(mpi::world, "Invalid aggregate info.");
                break;
            case IntraLevelType::Low:
                aggregate_map[key][level].low().add(aggr.second);
                break;
            case IntraLevelType::High:
                aggregate_map[key][level].high().add(aggr.second);
                break;
            case IntraLevelType::All:
                throw debug::NotImplementedException();
        }
    }

    std::map <KeyType, OnePassAggregate<Aggregate_>> finalize() {
        std::map <KeyType, OnePassAggregate<Aggregate_>> accumulator;

        for (auto &aggregate_element: aggregate_map) {
            KeyType key = aggregate_element.first;
            auto &aggregate_data = aggregate_element.second;

            // Add level 0 information (the coarsest level contributes with difference of zero.
            aggregate_data.at(0).high().finalize();
            accumulator[key] = aggregate_data.at(0).high().apply(interpolator, 0);

            for (int level = 1; level < (int) aggregate_data.size(); ++level) {
                auto & [aggregate_low, aggregate_high] = aggregate_data.at(level);

                aggregate_low.finalize();
                aggregate_high.finalize();

                if (level != 0) {
                    accumulator[key] += aggregate_high.apply(interpolator, level)
                                        - aggregate_low.apply(interpolator, level - 1);
                }
            }
        }

        return accumulator;
    }

    void write() {
        for (auto &elem: aggregate_map) {
            for (int level = 0; level < (int) elem.second.size(); ++level) {
                debug::log_info(mpi::world,
                                "Aggregate key = {}-high, level = {}, "
                                        "mean = {}, variance = {}, count = {}.",
                                elem.first, level,
                                elem.second.at(level).high().mean, elem.second.at(level).high().m2,
                                elem.second.at(level).high().count);
                if (level != 0) {
                    debug::log_info(mpi::world,
                                    "Aggregate key = {}-low, level = {}, "
                                            "mean = {}, variance = {}, count = {}.",
                                    elem.first, level,
                                    elem.second.at(level).low().mean, elem.second.at(level).low().m2,
                                    elem.second.at(level).low().count);
                }
            }
        }
    }

    auto &_get_map() const {
        return aggregate_map;
    }

    void reserve(int level, int key) {
        if (level >= nlevels) {
            set_levels(level + 1);
        }
        if (aggregate_map.count(key) == 0) {
            aggregate_map[key].resize(nlevels);
        }
    }

    void set_levels(int new_nlevels, bool warn = true) {
        if (new_nlevels < nlevels) {
            debug::log_warning(mpi::world,
                               "Reducing number of levels might incur in data loss!"
            );
        }
        if (warn) {
            debug::log_warning(mpi::world,
                               "Increasing/reducing number of levels from {} to {} might "
                                       "be expensive, consider preallocating levels!",
                               nlevels, new_nlevels
            );
        }
        nlevels = new_nlevels;
        for (auto &elem: aggregate_map) {
            elem.second.resize(nlevels);
        }
    }
private:
    std::map <KeyType, std::vector<lowHighPair<Aggregate_> >> aggregate_map;

    Interpolator interpolator;

    int nlevels = 0;
};

}



