/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 23/12/17.
//

namespace dafuq::base {

template<class Aggregate>
struct OnePassAggregate {
private:
    OnePassAggregate(const Aggregate &mean, const Aggregate &m2, int count, bool finalized = false)
            : mean(mean), m2(m2), count(count), finalized(finalized) {}

public:
    OnePassAggregate() = default;

    OnePassAggregate &operator+=(const OnePassAggregate &other) {
        if (!finalized || !other.finalized) {
            THROW_NOT_IMPLEMENTED;
        }

        mean += other.mean;
        m2 += other.m2;
        return *this;
    }

    OnePassAggregate operator-(const OnePassAggregate &other) const {
        if (!finalized || !other.finalized) {
            THROW_NOT_IMPLEMENTED;
        }

        return OnePassAggregate(mean - other.mean, m2 - other.m2, count, true);
    }

    template<class Function, class ...Args>
    OnePassAggregate apply(Function &&func, Args &&...args) {
        return OnePassAggregate(func(mean, args...), func(m2, args...), count, this->finalized);
    }

    void add(const Aggregate &aggr) {
        double n = ++count;

        auto delta = aggr - mean;
        mean += delta / n;
        auto delta2 = aggr - mean;

        m2 += delta * delta2;
    }

    void finalize() {
        if (finalized) {
            THROW_NOT_IMPLEMENTED;
        }
        finalized = true;
        if (count > 1) {
            m2 /= (count - 1.);
        } else {
            m2 = Aggregate();
        }
    }

    Aggregate mean, m2;
    int count = 0;
    bool finalized = false;
};

}