/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 13/12/17.
//

#include <type_traits>
#include <map>

#include <fmt/ostream.h>

#include <scheduler/JobInfo.hpp>

#include "user/IAggregate.hpp"

#include "OnePassAggregate.hpp"

namespace dafuq::base {

//! A generic Monte Carlo aggregator for simple computations.
//!
//!
//!
//! \tparam Aggregate_    A container for the data, which supports basic operations.
//! \tparam KeyType_      A key type storage of different aggregates (could index time, different variables, etc.).
template<class Aggregate_ = user::IAggregate, class KeyType_ = int>
class BasicAggregator {
public:
    using Aggregate = Aggregate_;

    using KeyType = KeyType_;

    static_assert(!std::is_integral_v<Aggregate>, "Aggregate cannot be of integral type, use floating point instead.");

    void add(const std::pair<KeyType, Aggregate> &aggr, const AggregateInfo &) {
        KeyType key = aggr.first;

        aggregate_map[key].add(aggr.second);
    }

    void finalize() {
        for (auto &elem: aggregate_map) {
            elem.second.finalize();
        }
    }

    void write() const {
        for (auto &elem: aggregate_map) {
            debug::log_info(mpi::world, "Aggregate key = {}, mean = {:.4}, variance = {:.4}, count = {}.",
                            elem.first, elem.second.mean, elem.second.m2, elem.second.count);
        }
    }

    auto & _get_map() const {
        return aggregate_map;
    }

    auto &get(const KeyType &key) {
        return aggregate_map.at(key);
    }
private:
    std::map<KeyType, OnePassAggregate<Aggregate>> aggregate_map;
};

}