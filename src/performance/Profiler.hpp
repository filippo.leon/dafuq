/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//! \file  Profiler.hpp
//! \brief File containing the Profiler class.
//!
//!
//!
//! \author Filippo Leonardi
//! \date   2018-01-01

#include <map>
#include <list>
#include <chrono>

#include "utilities/debug.hpp"

namespace dafuq::perf {

class Interval {
public:
    using Clock = std::chrono::steady_clock;
    using TimePoint = Clock::time_point;
    using Duration = std::chrono::duration<double>;

    Interval() {
        start_time = Clock::now();
    }

    void stop() {
        stop_time = Clock::now();
        stopped = true;
    }

    bool is_stopped() const {
        return stopped;
    }

    template<class TimePointDuration = std::chrono::milliseconds>
    typename TimePointDuration::rep get_start() const {
        return std::chrono::time_point_cast<TimePointDuration>(start_time).time_since_epoch().count();
    }

    template<class TimePointDuration = std::chrono::milliseconds>
    typename TimePointDuration::rep get_end() const {
        return std::chrono::time_point_cast<TimePointDuration>(stop_time).time_since_epoch().count();
    }

    Duration::rep length() const {
        return std::chrono::duration_cast<Duration>(stop_time - start_time).count();
    };
private:
    TimePoint start_time;
    TimePoint stop_time;
    bool stopped = false;
};

class Profiler {
public:
    Profiler(const std::string name) : name(name) {}

    void start_stage(const std::string &stage_name) {
        times[stage_name].push_back(Interval());
    }

    void end_stage(const std::string &stage_name) {
        if (times.empty() || times[stage_name].back().is_stopped()) {
            debug::log_warning(mpi::world, "Popping stage '{}' but list is empty or stage already popped!", stage_name);
        } else {
            times[stage_name].back().stop();
        }
    }

    const auto &get_map() const {
        return times;
    }

private:
    std::string name;

    std::map<std::string, std::list<Interval >> times;
};

}