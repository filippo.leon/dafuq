/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "options.hpp"

//
// Created by filippo on 04/12/17.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

namespace dafuq::io {

namespace po = boost::program_options;

Options options;

int Options::read(int argc, char** argv) {
    // Declare the supported options.
    options.desc.add_options()
            (
                    "help",
                    "Produce help message."
            )
            (
                    "version,v",
                    "Print version information."
            )
            (
                    "conf,c",
                    po::value<std::string>(),
                    "Specify configuration file."
            )
            (
                    "log_level",
                    po::value<int>(),
                    "Set logging level (trace = 0, debug = 1, info = 2, warning = 3, error = 4, fatal = 5)."
            );

    // Parse options
    try {
        po::store(po::parse_command_line(argc, argv, options.desc), options.vm);
    } catch(boost::program_options::unknown_option e) {
        debug::log_error<debug::LogPolicy::Collective>(mpi::world, "Boost ERROR {}", e.what());
        options.help(false);
    }

    po::notify(options.vm);

    // Read and notify config file
    std::string conf_file_path = DEFAULT_CONFIG_FILE;
    if( options.vm.count("conf") ) {
        conf_file_path = options.vm["conf"].as<std::string>();
    }
    std::ifstream file(conf_file_path);
    if( file ) {
        po::store(po::parse_config_file(file, options.desc, true), options.vm);
        po::notify(options.vm);
    }

    int log_level =  boost::log::trivial::info;
    if( options.vm.count("log_level") ) {
        log_level = options.as<int>("log_level");
    }

    boost::log::core::get()->set_filter(boost::log::trivial::severity >= log_level);

    // If help requested, exit
    if ( options.vm.count("help") ) {
        options.help();
    }

    if ( options.vm.count("version") ) {
        exit(0);
    }

    return 0;
}

void Options::help(bool abort) const {
    if (abort) {
        exit(0);
    }
}

}