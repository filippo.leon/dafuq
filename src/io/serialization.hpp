/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 12/12/17.
//

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "utilities/utilities.hpp"

namespace dafuq::serialization {

namespace pt = boost::property_tree;

inline void write_xml(const pt::ptree & tree, std::string filename) {
    boost::property_tree::xml_writer_settings<std::string> settings(' ', 4);

    pt::write_xml(filename, tree, std::locale(), settings);
}

template <class Job>
pt::ptree serialize_job(const Job & job) {
    pt::ptree tree;

    using precision = std::chrono::milliseconds;

    tree.put("id", job.get_id());
    tree.put("assign_time", std::chrono::time_point_cast<precision>(job.assigned_time).time_since_epoch().count());
    tree.put("complete_time", std::chrono::time_point_cast<precision>(job.complete_time).time_since_epoch().count());
    tree.put("ranks", utilities::to_string(job.get_assigned_ranks()));

    return tree;
}

template <class JobPool>
void serialize_job_pool(pt::ptree &tree, const JobPool &job_pool) {
    pt::ptree pool_tree;

    for(auto& job: job_pool.get_jobs()) {
        pool_tree.add_child("Job", serialize_job(job));
    }
    pool_tree.put("<xmlattr>.size", job_pool.get_jobs().size());
    tree.add_child("JobPool", pool_tree);
}


void serialize_profiler(pt::ptree &tree, const perf::Profiler &profiler) {
    pt::ptree profiler_tree;

    const auto &map = profiler.get_map();
    for (auto &stage: map) {
        pt::ptree stage_tree;

        for (auto &interval: stage.second) {
            pt::ptree interval_tree;

            interval_tree.put("<xmlattr>.start", interval.get_start<>());
            interval_tree.put("<xmlattr>.end", interval.get_end<>());
            interval_tree.put("<xmlattr>.duration", interval.length());
            stage_tree.add_child("Interval", interval_tree);
        }
        stage_tree.put("<xmlattr>.name", stage.first);
        stage_tree.put("<xmlattr>.size", stage.second.size());
        profiler_tree.add_child("Stage", stage_tree);
    }
    profiler_tree.put("<xmlattr>.number", map.size());
    tree.add_child("Stages", profiler_tree);
}

template<class Scheduler>
pt::ptree serialize_scheduler(const Scheduler &scheduler) {
    pt::ptree tree;

    auto &root = tree.add_child("Scheduler", pt::ptree{});

    serialize_job_pool(root, scheduler.get_jobs_pool());

    serialize_profiler(root, scheduler.get_profiler());

    return tree;
}

}