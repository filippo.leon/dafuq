/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 04/12/17.
//

#include "utilities/debug.hpp"
#include "utilities/mpi.hpp"

#include "ISolverInfo.hpp"

#include "scheduler/Job.hpp"

#include "user/ISolverInfo.hpp"
#include "user/ISolverConfig.hpp"
#include "user/IReceiver.hpp"

namespace dafuq::user {

template<class SolverInfo_ = ISolverInfo<>,
        class SolverConfig_ = ISolverConfig,
        class Aggregate_ = IAggregate,
        class Receiver_ = IReceiver<Aggregate_>>
class ISolver {
public:
    using SolverInfo = SolverInfo_;
    using SolverConfig = SolverConfig_;
    using Receiver = Receiver_;
    using Aggregate = Aggregate_;

    using Job = dafuq::base::Job<SolverInfo>;

    virtual ~ISolver() = default;

    void configure() { }

    /*Solution*/void start(const base::Job<SolverInfo> & job) {
        throw debug::NotImplementedException();
    }

    /*SolverCost*/double get_cost() const {
        throw debug::NotImplementedException();
    }

    bool can_receive() {
        throw debug::NotImplementedException();
    }

    void send(const base::Job<SolverInfo> & job, int tag) {
        throw debug::NotImplementedException();
    }

    static void receive(int tag) {
        throw debug::NotImplementedException();
    }

};

}