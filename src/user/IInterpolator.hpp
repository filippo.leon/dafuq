/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 13/12/17.
//

namespace dafuq::user {

//! \brief Template for an interpolator Functor.
//!
//! This operator is a valid Interpolator which performs no operation: \f$ I(x) = x \f$ (identity operator).
//!
//! \warning This class should be used as a template, as it is very likely that an interpolator is required
//!          in multi-level scenarios (except maybe if there is an implict conversion between levels,
//!          such as double-single precision multi level).
//!
//! \todo Consider specifying different types on different levels, and then ask the user to provide explicit cast
//!       operators to upscale the data.
//!
//! \see \ref interpolator
//!
//! \tparam Aggregate The aggregate type which will be interpolated.
template<class Aggregate>
class IInterpolator {
public:
    //! \brief Function-call syntax of the Interolator.
    //!
    //! This function is just a dummy and returns the data as-is.
    //!
    //! \param low  The data in the coarses state, to be upscaled to the next level.
    //! \return     The data interpolated to the finest level from the coarsest 'level'.
    Aggregate operator()(const Aggregate &low, int /*level*/) {
        return low;
    }
};

}