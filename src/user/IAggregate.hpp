/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 13/12/17.
//

namespace dafuq::user {

class IAggregate;

//! \brief Send the Aggregate to the output stream.
//!
//! This method is used to output certain messages about the Aggregate.
//! If you provide this overload, then the aggregate will be printed as debugging information
//! This can be left blank.
//!
//! \param o        Output stream
//! \param aggr     The aggregate to output
//! \return         The output stream.
std::ostream& operator<<(std::ostream & o, const IAggregate & aggr);

//! \brief An Aggregate will contain the solution data of a Solver, whenever received by the Collector
//!        via a \ref receiver implementation.
//!
//! An aggregate will contain some (possibly heavy) data, which represents a solution of one (or multiple)
//! solver runs. It might contain data which is batched, a level difference or weighted data (or a combination
//! thereof).
//!
//! It also provides a few necessary methods for the manipulation of the data.
//!
//! \warning This class is empty and just a placeholder. In order to customize the behaviour of this
//!          class, you should implement all methods of this class.
//!
//! \see \ref aggregate, \ref receiver, \ref Collector, \ref IReceiver
//!
class IAggregate {
public:
    //! \brief Multiply two aggregates and return the result.
    //!
    //!
    //!
    //! \return
    IAggregate operator*(const IAggregate &) const {
        return IAggregate();
    }

    //! \brief Subtract two aggregates and return the result.
    //!
    //!
    //!
    //! \return
    IAggregate operator-(const IAggregate &) const {
        return IAggregate();
    }

    //! \brief Add another aggretate to this, return reference to this.
    //!
    //!
    //!
    //! \return
    IAggregate &operator+=(const IAggregate &) {
        return *this;
    }

    //! \brief Create a new Aggregate which is this aggregate divided by a factor.
    //!
    //!
    //!
    //! \return
    IAggregate operator/(double) const {
        return IAggregate();
    }

    //! \brief Divide aggregate by a scalar factor.
    //!
    //!
    //!
    //! \return
    IAggregate &operator/=(double) {
        return *this;
    }

    friend std::ostream &operator<<(std::ostream &o, const IAggregate &);
};

std::ostream &operator<<(std::ostream &o, const IAggregate &) {
    return o;
}

}