/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 13/12/17.
//

#include "utilities/debug.hpp"

namespace dafuq::user {

//! \brief Receive data from the Executor to the Collector.
//!
//! This class is in charge of receiving data from the \ref Executor Group into the \ref Collector Group
//! and collect the data so to create an \ref aggregate for further manipulation.
//!
//! The caller will provide an MPI tag, communicator with which communication should be done, and it will
/// also provide the \ref Job whose data is being received.
//!
//! \warning This is a dummy class and should be re-implemented to receive custom data.
//!
//! \see \ref receiver
//!
//! \tparam Aggregate   The Aggregate type that will be constructed and manipulated in the Collector Group.
//! \tparam KeyType     The key type used to indicise multiple sends by the same \ref solver.
template<class Aggregate, class KeyType = int>
class IReceiver {
public:
    //! \brief Receive data from a Job.
    //!
    //! The called provides the job information, a tag for MPI communication, and a communicator for the communication.
    //! The job contains the ranks of the \ref Execution Group associated with the Job.
    //!
    //! \tparam Job     The type of the Job that is sending the data.
    //! \return         A pair with a key and the data received.
    template<class Job>
    std::pair<KeyType, Aggregate> receive(const Job & /*job*/, int /*tag*/, const mpi::Communicator & /*comm*/) {
        return {0, Aggregate()};
    }
};

}