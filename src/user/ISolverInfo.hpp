/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 11/12/17.
//

#include <mpi.h>

namespace dafuq::user {

template<class Derived = void>
class ISolverInfo {
public:
    static MPI_Datatype register_mpi_type() {
        return mpi_type;
    };

    static MPI_Datatype get() {
        if constexpr(is_void) {
            throw debug::DafuqException("You should not use the MPI_Datatype provided by an empty ISolverInfo.");
        }
        return mpi_type;
    }

    static bool is_registered() {
        return true;
    }

    static MPI_Datatype mpi_type;

    static constexpr bool is_void = std::is_same_v<Derived, void>;
};

template<class Derived>
MPI_Datatype ISolverInfo<Derived>::mpi_type;

}