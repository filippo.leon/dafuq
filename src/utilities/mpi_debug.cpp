/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//
// Created by filippo on 07-Dec-17.
//

#include "mpi_debug.hpp"

#include <mpi.h>

#include "debug.hpp"

namespace dafuq::debug {

std::string mpi_error_decode(int error_code) {
    constexpr int BUFFER_SIZE = MPI_MAX_ERROR_STRING;
    char error_string[BUFFER_SIZE];
    int length;

    MPI_Error_string(error_code, error_string, &length);

    return std::string(error_string, length);
}

MPIException::MPIException(int errcode)
            : errcode(errcode) {
    LOG(error) << what();
}

MPIException::MPIException(int errcode, int line, std::string file)
            : errcode(errcode), line(line), file(file) {
    LOG(error) << what();
}

const char * MPIException::what() const noexcept {
    if( line != -1 ) {
        what_str = debug::error(mpi::world, "MPI Error {} at line {} of {}: {}",
                            errcode, line, file, mpi_error_decode(errcode)
        );
    } else {
        what_str = debug::error(mpi::world, "MPI Error {}: {}",
                            errcode, mpi_error_decode(errcode)
        );
    }
    return what_str.c_str();
}

}
