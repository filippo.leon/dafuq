/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once
//
// Created by filippo on 04/12/17.
//

#include <vector>

#include <mpi.h>

#include "mpi.hpp"

namespace dafuq::mpi {

template<typename T>
struct datatype;

template<>
struct datatype<int> {
    static constexpr MPI_Datatype value = MPI_INT;
};

template<>
struct datatype<double> {
    static constexpr MPI_Datatype value = MPI_DOUBLE;
};

template<class T>
void bcast(T &val, int root, const mpi::Communicator &comm = mpi::world) {
    mpiSafeCall(
            MPI_Bcast(&val, 1, mpi::datatype<T>::value, root, comm)
    );
}

template<template<typename T> class Container, typename T>
void bcast(Container<T> &container, int root, const mpi::Communicator &comm = mpi::world) {
    T *buffer;
    int size;

    if (comm.get_rank() == root) {
        buffer = container.data();
        size = container.size();
    }
    bcast(size, root, comm);
    if (comm.get_rank() != root) {
        container.resize(size);
        buffer = container.data();
    }

    mpiSafeCall(
            MPI_Bcast(buffer, size, mpi::datatype<T>::value, root, comm)
    );
}

template<class T>
void send(const T &val, int count, int dest, int tag, const mpi::Communicator &comm) {
    if (!T::is_registered()) {
        debug::log_error(comm, "MPI_Datatype not registered for this type!");
    }
    mpiSafeCall(MPI_Send(&val, 1, val.get(), dest, tag, comm));
}

template<class T>
mpi::Status recv(T &val, int count, int source, int tag, const mpi::Communicator &comm) {
    mpi::Status status;

    if (!T::is_registered()) {
        debug::log_error(comm, "MPI_Datatype not registered for this type!");
    }
    mpiSafeCall(MPI_Send(&recv, 1, val.get(), source, tag, comm, &status.get()));

    return status;
}

}