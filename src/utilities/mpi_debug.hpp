/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 07-Dec-17.
//

#include <string>

namespace dafuq::debug {

std::string mpi_error_decode(int error_code);

class MPIException : std::exception {
public:
    MPIException(int errcode);

    MPIException(int errcode, int line, std::string file);

    const char * what() const noexcept;
private:
    int errcode;
    int line = -1;
    std::string file;
    mutable std::string what_str;
};

}