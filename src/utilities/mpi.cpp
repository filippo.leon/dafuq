/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//
// Created by filippo on 04/12/17.
//

#include <mpi.h>

#include "mpi.hpp"

#include "debug.hpp"

#include "utilities/utilities.hpp"

namespace dafuq::mpi {

Communicator::Communicator(const std::vector<int> & ranks,
                           const std::string & name, const mpi::Communicator & base)
    : name(name) {
    MPI_Group group, base_group;
    MPI_Comm_group(base, &base_group);
    MPI_Group_incl(base_group, (int) ranks.size(), ranks.data(), &group);

    mpiSafeCall(MPI_Comm_create_group(base, group, 0, &comm));

    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);
}

Communicator::Communicator(MPI_Comm comm, const std::string & name)
        : name(name), comm(comm) {
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);
}

Communicator::operator MPI_Comm() const {
    return comm;
}

int Communicator::get_size() const {
    return size;
}

int Communicator::get_rank() const {
    return rank;
}

bool Communicator::is_root() const {
    return rank == 0;
}

const std::string & Communicator::get_name() const {
    return name;
}

Communicator world;

void SafeCall(int errcode, int line, std::string file) {
    if( errcode != MPI_SUCCESS ) {
        throw debug::MPIException(errcode, line, file);
    }
}

void terminate() {
    finalize();
    exit(debug::exit_failure);
}

void init(int* argc, char*** argv) {
    int provided = MPI_THREAD_SINGLE;
    mpiSafeCall(MPI_Init_thread(argc, argv, MPI_THREAD_MULTIPLE, &provided));
    if (provided != MPI_THREAD_MULTIPLE) {
        debug::log_error(mpi::world, "No MPI Thread Multiple available!");
        terminate();
    }

    world = Communicator(MPI_COMM_WORLD, "world");
}

int finalize() {
    return MPI_Finalize();
}

}