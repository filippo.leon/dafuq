/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once
//
// Created by filip on 07-Dec-17.
//

#include <boost/log/trivial.hpp>

#include <fmt/format.h>

#include "utilities/mpi.hpp"

#define LOG(x) BOOST_LOG_TRIVIAL(x)

namespace dafuq::debug {

namespace color {

using namespace std::string_literals;

const std::string red     = "\x1b[31;1m";
const std::string yellow  = "\x1b[33m";
const std::string green   = "\x1b[32;1m";
const std::string magenta = "\x1b[35;1m";
const std::string reset   = "\x1b[0m";

const char blue[] = "\x1b[34;1m";

} // END namespace color

class DafuqException
        : public std::exception {
public:
    DafuqException() = default;

    DafuqException(std::string msg) {
        what_str = msg;
    }

protected:
    mutable std::string what_str;
};

class NotImplementedException
        : public DafuqException {
public:
    NotImplementedException() = default;

    NotImplementedException(int line, std::string file)
        : line(line), file(file) { }

    const char * what() const noexcept {
        if( line == -1 ) {
            what_str = "Feature not implemented at unknown line and unknown file.";
        } else {
            what_str = fmt::format("{}Feature not implemented{} at line {} in file {}.",
                                   debug::color::red, debug::color::reset, line, file);
        }
        return what_str.c_str();
    }

protected:

    int line = -1;

    std::string file;
};

#define THROW_NOT_IMPLEMENTED throw dafuq::debug::NotImplementedException(__LINE__, __FILE__)

const std::string error_string    = color::red     + "[E ";
const std::string warning_string  = color::yellow  + "[W ";
const std::string info_string     = color::green   + "[I ";
const std::string verbose_string  = color::magenta + "[V ";

const std::string reset_string    = "] " + color::reset;

enum class LogPolicy {
    Collective,
    Self,
};

template <class ...Args>
std::string error(const mpi::Communicator & comm, const std::string& format, Args&&... args) {
    return fmt::format("{}{} {}/{}{}" + format, error_string,
                       comm.get_name(), comm.get_rank(), comm.get_size(), reset_string, args...);
}

template <LogPolicy policy = LogPolicy::Self, class ...Args>
void log_error(const mpi::Communicator & comm, const std::string& format, Args&&... args) {
    if (policy == LogPolicy::Self or (policy == LogPolicy::Collective and comm.get_rank() == 0)) {
        LOG(error) << error(comm, format, args...);
    }
}

template <class ...Args>
std::string warning(const mpi::Communicator & comm, const std::string & format, Args&&... args) {
    return fmt::format("{}{} {}/{}{}" + format, warning_string,
                       comm.get_name(), comm.get_rank(), comm.get_size(), reset_string, args...);
}

template <LogPolicy policy = LogPolicy::Self, class ...Args>
void log_warning(const mpi::Communicator & comm, const std::string& format, Args&&... args) {
    if (policy == LogPolicy::Self or (policy == LogPolicy::Collective and comm.get_rank() == 0)) {
        LOG(warning) << warning(comm, format, args...);
    }
}

template <class ...Args>
std::string info(const mpi::Communicator & comm, const std::string & format, Args&&... args) {
    return fmt::format("{}{} {}/{}{}" + format, info_string,
                       comm.get_name(), comm.get_rank(), comm.get_size(), reset_string, args...);
}

template <LogPolicy policy = LogPolicy::Self, class ...Args>
void log_info(const mpi::Communicator & comm, const std::string& format, Args&&... args) {
    if (policy == LogPolicy::Self or (policy == LogPolicy::Collective and comm.get_rank() == 0)) {
        LOG(info) << info(comm, format, args...);
    }
}

template <class ...Args>
std::string verbose(const mpi::Communicator & comm, const std::string & format, Args&&... args) {
    return fmt::format("{}{} {}/{}{}" + format, verbose_string,
                       comm.get_name(), comm.get_rank(), comm.get_size(), reset_string, args...);
}

template <class ...Args>
void log_verbose(const mpi::Communicator & comm, const std::string& format, Args&&... args) {
    LOG(debug) << verbose(comm, format, args...);
}

inline constexpr int exit_failure = -1;

}