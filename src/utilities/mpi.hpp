/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once
//
// Created by filippo on 04/12/17.
//

#include <vector>

#include <mpi.h>

#include "mpi_debug.hpp"

#define mpiSafeCall(x) \
dafuq::mpi::SafeCall(x, __LINE__, __FILE__);

namespace dafuq::mpi {

void SafeCall(int errcode, int line, std::string file);

class Communicator;
extern Communicator world;

class Communicator {
public:
    Communicator() = default;

    explicit Communicator(const std::vector<int> & ranks, const std::string & name = "unknown",
                 const mpi::Communicator & base = world);

    Communicator(MPI_Comm comm, const std::string & name = "unknown");

    operator MPI_Comm() const;

    int get_size() const;

    int get_rank() const;

    bool is_root() const;

    const std::string & get_name() const;

    Communicator duplicate() const {
        MPI_Comm newcomm;
        MPI_Comm_dup(comm, &newcomm);
        return Communicator(newcomm, name + "-dup");
    }

    Communicator split(int color, int key) const {
        MPI_Comm newcomm;
        MPI_Comm_split(comm, color, key, &newcomm);
        return Communicator(newcomm, name + "-sub" + std::to_string(color));
    }
private:
    std::string name = "null";

    MPI_Comm comm = MPI_COMM_NULL;

    int rank = 0, size = 0;
};

void init(int* argc, char*** argv);

int finalize();

class Status {
public:
    Status() {
        s.MPI_ERROR = MPI_SUCCESS;
    }
    Status(MPI_Status s) : s(s) {
        this->s.MPI_ERROR = MPI_SUCCESS;
    }

    MPI_Status & get() {
        return s;
    }

    int tag() const {
        return s.MPI_TAG;
    }

    int source() const {
        return s.MPI_SOURCE;
    }

    void check() const {
        if( s.MPI_ERROR != MPI_SUCCESS ) {
            throw debug::MPIException(s.MPI_ERROR);
        }
    }

private:
    MPI_Status s;
};

class Request {
public:
    Request() = default;
    Request(MPI_Request r) : r(r) { }

    MPI_Request & get() {
        return r;
    }

    void wait() {
        Status status;
        mpiSafeCall(MPI_Wait(&r, &status.get()));
        status.check();
    }

private:
    MPI_Request r;
};

}