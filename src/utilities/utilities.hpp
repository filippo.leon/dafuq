/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 13/12/17.
//

#include <iostream>
#include <sstream>
#include <algorithm>
#include <numeric>

namespace  dafuq::utilities {

inline std::string to_string(const std::vector<int> & v) {
    std::stringstream ss;
    copy( v.begin(), v.end(), std::ostream_iterator<int>(ss, " "));
    return ss.str();
}

inline std::vector<int> range(int start, int size) {
    std::vector<int> vec(size);
    std::iota(vec.begin(), vec.end(), start);
    return vec;
}

template<typename T>
inline bool equality(const T &a, const T &b) {
    return a == b;
}

template<class Container, class Predicate = decltype(equality<typename Container::value_type>)>
inline bool are_equal(const Container &a, const Container &b,
                      const Predicate &P = equality<typename Container::value_type>) {
    if (a.size() != b.size()) return false;

    for (int i = 0; i < (int) a.size(); ++i) {
        if (!P(a[i], b[i])) return false;
    }

    return true;

}

class ignore_ {
};

}
