/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "well512a.hpp"

//
// Created by filippo on 12/12/17.
//

namespace dafuq::random {

#define WELL512a_R 16

/*** Well512a wrapper from http://www.iro.umontreal.ca/~panneton/well/WELL512a.c ***/
extern "C" {

void InitWELLRNG512a(unsigned int *init);

double WELLRNG512a(void);

}

void well512a::seed(int s) {
    unsigned int buffer[WELL512a_R];

    // init buffer using Linear Congruential Generator (from ALSVID-UQ)
    const long long a = 1103515245;
    const long long c = 12345;

    unsigned int x = s;

    for (int i = 0; i < WELL512a_R; i++) {
        x = a * x + c;
        buffer[i] = x;
    }

    InitWELLRNG512a(buffer);
}

well512a::result_type well512a::operator()() {
    return WELLRNG512a();
}

void well512a::discard(unsigned long long z) {
    for (unsigned long long z1 = 0; z1 < z; ++z1) {
        (*this)();
    }
}

}
