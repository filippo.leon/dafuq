/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 01/01/14.
//

#include <functional>
#include <array>
#include <vector>
#include <random>
#include <chrono>

#include "utilities/mpi.hpp"
#include "utilities/debug.hpp"

namespace dafuq::random {

using DefaultMasterEngine = std::default_random_engine;
using DefaultMasterDistribution = std::uniform_int_distribution<int>;

using DefaultEngine = std::default_random_engine;
using DefaultDistribution = std::uniform_real_distribution<double>;

template<class Engines = std::tuple<DefaultEngine>,
        class Distributions = std::tuple<DefaultDistribution>,
        class MasterEngine = DefaultMasterEngine,
        class MasterDistribution = DefaultMasterDistribution>
class Sampler {
public:
    static_assert(std::is_integral_v<typename MasterDistribution::result_type>,
                  "Master Distribution should return integral types!");
    static_assert(std::tuple_size<Engines>::value == std::tuple_size<Distributions>::value,
                  "Engines and Distributions must have the same size!");

    static constexpr unsigned int N = std::tuple_size<Engines>::value;

    Sampler(int master_seed = -1)
            : master_seed{master_seed} {
        if (master_seed == -1) {
            this->master_seed = {
                    (int) getpid(),
                    (int) std::chrono::duration_cast<std::chrono::seconds>(
                            std::chrono::high_resolution_clock::now().time_since_epoch()
                    ).count(),
                    (int) std::hash<std::thread::id>{}(std::this_thread::get_id())
            };
        }

//        mpi::broadcast(this->master_seed, mpi::world);
        mpiSafeCall(MPI_Bcast(this->master_seed.data(), 3, MPI_INT, 0, mpi::world));

        debug::log_info<debug::LogPolicy::Collective>(
                mpi::world,
                "Initializing sampler with master seed '{}'", utilities::to_string(this->master_seed)
        );

        std::seed_seq s(this->master_seed.begin(), this->master_seed.end());
        master_engine.seed(s);
    }

    template<unsigned int I, class ...Args>
    void configure(const Args &...args) {
        std::get<I>(distributions) = typename std::tuple_element<I, Distributions>::type(args...);
    }

    void set_index(int index) {
        current_index = index;

        reseed_engines<N>();
    }

    template<unsigned int I>
    typename std::tuple_element_t<I, Distributions>::result_type get(int k) {
        if (k >= (int) std::get<I>(entropy).size()) {
            debug::log_info(mpi::world,
                            "Trying to read element {} of dimension {}, but reserved size was {}.",
                            k, I, std::get<I>(entropy).size()
            );
            resize_entropy(k + 1);
        }
        return std::get<I>(entropy).at(k);
    }

    template<unsigned int I>
    typename std::tuple_element_t<I, Distributions>::result_type next() {
        if (std::get<I>(current_k) >= std::get<I>(entropy).size()) {
            debug::log_info(mpi::world,
                            "Trying to read element {} of dimension {}, but reserved size was {}.",
                            std::get<I>(current_k), I, std::get<I>(entropy).size()
            );
            resize_entropy(std::get<I>(current_k) + 1);
        }
        return std::get<I>(entropy).at(std::get<I>(current_k)++);
    }

    void resize_entropy(int size) {
        if (size >= (int) std::get<0>(entropy).size()) {
            if (engines_dirty) {
                reseed_engines<N>();
            }

            generate_entropy<N>(size);
        }
    }

private:
    template<unsigned int I>
    std::enable_if_t<I == 0> generate_entropy(int /*size*/) {
        engines_dirty = true;
    }

    template<unsigned int I>
    std::enable_if_t<I != 0> generate_entropy(int size) {
        std::get<I - 1>(entropy).resize(size);
        std::get<I - 1>(entropy).resize(std::get<I - 1>(entropy).capacity());

        for (auto &val: std::get<I - 1>(entropy)) {
            val = std::get<I - 1>(distributions)(std::get<I - 1>(engines));
        }

        generate_entropy<I - 1>(size);
    }

    template<unsigned int I>
    std::enable_if_t<I == 0> reseed_engines() {
        engines_dirty = false;
    }

    template<unsigned int I>
    std::enable_if_t<I != 0> reseed_engines() {
        std::get<I - 1>(engines).seed(get_seed<I - 1>(current_index.value()));

        std::get<I - 1>(current_k) = 0;

        reseed_engines<I - 1>();
    }

    template<unsigned int I>
    std::enable_if_t<I == 0> resize_and_generate_seeds(int /*size*/) {}

    template<unsigned int I>
    std::enable_if_t<I != 0> resize_and_generate_seeds(int size) {
        std::get<I - 1>(seeds).resize(size);
        // Embiggen to capacity
        std::get<I - 1>(seeds).resize(std::get<I - 1>(seeds).capacity());

        resize_and_generate_seeds<I - 1>(size);

        if constexpr(I == N) {
            for (auto &seeds_vector: seeds) {
                for (int &s: seeds_vector) {
                    s = master_distribution(master_engine);
                }
            }
        }
    }

    template<unsigned int I>
    int get_seed(int index) {
        if (index >= (int) std::get<0>(seeds).size()) {
            debug::log_info(mpi::world,
                            "Trying to read seed {}, but reserved size was {}.",
                            index, std::get<0>(seeds).size()
            );
            // Reseed to make sure the same cycle start is used
            std::seed_seq s(this->master_seed.begin(), this->master_seed.end());
            master_engine.seed(s);

            resize_and_generate_seeds<N>(index + 1);
        }

        return std::get<I>(seeds).at(index);
    }

private:
    //! The seed shared globally among all ranks, providing all other seed for simulations
    std::vector<int> master_seed = {-1};

    std::array<std::vector<int>, N> seeds;

    MasterDistribution master_distribution;
    MasterEngine master_engine;

    mutable std::optional<int> current_index;

    Engines engines;
    Distributions distributions;

    std::array<int, N> current_k;

    template<class>
    class tuple_of_vectors_of_result_types_of;

    template<template<typename...Args> class Tuple, typename ...Arguments>
    struct tuple_of_vectors_of_result_types_of<Tuple<Arguments...>> {
        using type = std::tuple<std::vector<typename Arguments::result_type>...>;
    };

    typename tuple_of_vectors_of_result_types_of<Distributions>::type entropy;

    mutable bool engines_dirty = true;
};

}
