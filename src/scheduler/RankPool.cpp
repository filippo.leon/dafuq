/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//
// Created by filippo on 11/12/17.
//

#include "RankPool.hpp"

namespace dafuq::base {

RankPool::RankPool(const mpi::Communicator & comm) {
    // Initially no rank is used.
    ranks.resize((std::size_t) comm.get_size(), NO_RANK);
}

std::vector<ContiguousRankGroup> RankPool::get_free_rank_groups() {
    std::vector<ContiguousRankGroup> groups;
    debug::log_verbose(mpi::world, "Looking for free rank groups.");

    // Loop from start to finish for gaps
    int count = 0;
    for (int i = 0; i <= (int) ranks.size(); ++i) {
        // Count contiguous free ranks
        if (i != (int) ranks.size() and ranks[i] == NO_RANK) {
            ++count;
            // Gap ended, try and fill as much as possible
        } else {
            // Starting index of current gap
            int gap_start = i - count;

            if (count > 0) {
                debug::log_verbose(mpi::world, "Found contiguous group starting at {} with size {}.", gap_start, count);
                groups.emplace_back(ContiguousRankGroup(gap_start, count));
            }

            count = 0;
        }
    }

    return groups;
}

void RankPool::unassign(const ContiguousRankGroup & group) {
    for(int rk: group) {
        ranks[rk] = NO_RANK;
    }
}

}