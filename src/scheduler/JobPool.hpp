/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 11/12/17.
//

#include <mutex>
#include <optional>

namespace dafuq::base {

template<class Job>
class JobPool {
public:
    JobPool(const mpi::Communicator &comm) : comm(comm) {

    }

    //! A JobPool is deleted due to it resetting the Job counter
    JobPool(const JobPool &other) = delete;

    ~JobPool() {
        Job::reset_id_counter();
    }

    void add_job(const Job & job) {
        std::lock_guard lock(mutex);

        jobs.push_back(job);

        available_jobs.insert(std::make_pair<int, int>(job.get_min_ranks(), job.get_id()));
    }

    std::vector<int> wait_for_job_complete() {
        int id;
        try {
            id = Job::wait_for_job_complete(comm);
        } catch (...) {
            return std::vector<int>();
        }

        std::lock_guard lock(mutex);
        Job *job = &jobs.at(id);
        job->set_complete();

        return job->get_assigned_ranks();
    }

    //! \brief Remove job from list of available jobs.
    //!
    //!
    //!
    //! \tparam Group
    //! \param job
    //! \param grp
    template <class Group>
    void assign_job(int id, Group /*grp*/) {
        std::lock_guard lock(mutex);

        auto& job = jobs.at(id);

        assert( id == job.get_id() );
        assert( job.is_assigned() && "Job was not assigned!" );

        for (auto[itb, ite] = available_jobs.equal_range(job.get_min_ranks()); itb != ite; ++itb) {
            if( itb->second == job.get_id() ) {
                available_jobs.erase(itb);
                return;
            }
        }

        debug::log_error(mpi::world, "Job {} was not found in map of available jobs!", job.get_id());
    }

    bool is_empty() {
        std::lock_guard lock(mutex);

        return available_jobs.empty();
    }

    std::optional<int> get_biggest_job_of_size(int size) {
        std::lock_guard lock(mutex);

        auto it = available_jobs.lower_bound(size);
        if( it != available_jobs.end() ) {
            int id = it->second;

            if (id >= (int) jobs.size()) {
                debug::log_error(mpi::world, "Invalid job Id {} of {}", id, jobs.size());
            }

            debug::log_info(mpi::world, "Found job {} (biggest of size <= {}), with min. ranks {}!",
                            jobs[id].get_id(), size, jobs[id].get_min_ranks());
            return id;
        }

        return std::optional<int>();
    }

    //! \brief Integrity check with number of jobs yet to complete.
    //!
    //!
    void _assert_all_complete() const {
        assert(available_jobs.size() == 0 && "Unassigned jobs detected!");
        assert(incomplete_count() == 0 && "Incomplete jobs detected!");
    }

    //!
    //! \return
    int incomplete_count() const {
        std::lock_guard lock(mutex);

        int count = 0;

        for(auto& job: jobs) {
            if( !job.is_complete() ) {
                ++count;
            }
        }

        debug::log_info(mpi::world, "Total jobs {}, incomplete {}, available {}.",
                           jobs.size(), count, available_jobs.size());

        return count;
    }

    bool are_all_complete() const {
        return incomplete_count() == 0;
    }

    const auto& get_jobs() const {
        return jobs;
    }

    auto& get_jobs() {
        return jobs;
    }

    mutable std::mutex mutex;
private:
    mpi::Communicator comm;

    std::vector<Job> jobs;

    std::multimap<int, int, std::greater<int>> available_jobs;
};

}