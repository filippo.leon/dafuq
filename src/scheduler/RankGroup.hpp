/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 11/12/17.
//

#include "utilities/debug.hpp"
#include "utilities/utilities.hpp"

namespace dafuq::base {

class RankGroup {

private:

};

class ContiguousRankGroup : public RankGroup {
public:
    ContiguousRankGroup(int start = 0, int size = 0)
        : start(start), size(size) {

    }

    template <class Container>
    ContiguousRankGroup(const Container & container) {
        auto old_it = container.end();
        for(auto it = container.begin(); it != container.end(); ++it) {
            if( old_it != container.end() ) {
                assert(*old_it + 1 == *it && "Values must be consecutive!");
            }
            old_it = it;
        }
        start = *(container.begin());
        size = container.end() - container.begin();
    }

    class iterator {
    public:
        iterator(int start) {
            val = start;
        }

        bool operator==(const iterator &other) const {
            return val == other.val;
        }

        bool operator!=(const iterator &other) const {
            return !(*this == other);
        }


        int &operator*() {
            return val;
        }

        const int &operator*() const {
            return val;
        }

        iterator &operator++() {
            ++val;
            return *this;
        }

        iterator operator--() {
            --val;
            return *this;
        }

    private:
        int val;
    };

    iterator begin() const {
        return iterator(start);
    }

    iterator end() const {
        return iterator(start + size);
    }

    ContiguousRankGroup split(int count) {
        int old_start = start;
        start += count;
        size -= count;

        return ContiguousRankGroup(old_start, count);
    }

    int get_size() const {
        return size;
    }

    std::vector<int> get_vector() {
        return utilities::range(start, size);
    }
private:
    int start = 0, size = 0;
};

}