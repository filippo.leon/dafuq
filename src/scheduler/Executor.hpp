/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 04/12/17.
//

#include "utilities/debug.hpp"

namespace dafuq::base {

//! \brief The Executor class is in charge or handling jobs sent by the Scheduler.
//!
//! The job of this class is to wait for a job sent from the Scheduler and
//! then execute it, and finally send the generated data to the Collector.
//!
//! \see \ref Collector, \ref Scheduler,
//!      \ref scheduler_structure, \ref execution_group
//!
//! \tparam Scheduler    The type of the parent scheduler.
template<class Scheduler>
class Executor {
public:
    friend Scheduler; //!< Access to private members of scheduler.

    //! Alias for the Solver type (provided by the Scheduler).
    using Solver = typename Scheduler::Solver;
    //! Alias for the Job type (provided by the Solver).
    using SolverJob = typename Scheduler::SolverJob;
    //! Alias for the SolverConfig type (provided by the Solver).
    using SolverConfig = typename Solver::SolverConfig;
    //! Alias for the Collector type (provided by the Scheduler).
    using Collector = typename Scheduler::SchedulerCollector;

    //! \breif Sets up an executor
    //!
    //!  Binds the scheduler to this.
    //!
    //! \param scheduler    Parent scheduler
    //! \param levels       \deprecated Number of levels of the simulation.
    Executor(Scheduler &scheduler, int levels)
            : scheduler(scheduler), levels(levels) {}

    //! \brief Start the Execution loop.
    //!
    //! The loop is started in a separate thread if the rank is a Collector.
    void start() {
        if (scheduler.get_collector().is_collector()) {
            debug::log_info(scheduler.get_comm(),
                            "Starting job Executor in a separate thread.");
            thr = std::thread(&Executor::wait_for_job_and_execute, this);
        } else {
            debug::log_info(scheduler.get_comm(),
                            "Starting job Executor in main thread.");
            wait_for_job_and_execute();
        }
    }

    //! Wait for the Execution loop to terminate.
    //!
    //! After this thr is no longer joinable.
    void wait() {
        if (thr.joinable()) {
            thr.join();
        }
    }

protected:
    //! \brief Main loop: wait for a job and run the Solver.
    //!
    //! The job is sent from the scheduler. If a job is received and the job
    //! is valid, then the Solver will be executed for that job.
    //!
    //! Once the solver is done, we send the data to the collector (the Exeutor
    //! will provide a tag to the Solver, and the Solver himself will send the
    //! data).
    //!
    //! After this the Executor will send a 'complete' message to the Scheduler
    //! to notify of the finished job.
    //!
    //! \todo Implement/Test start and stop of the solver to allow
    //!       send of intermediate
    //!       results.
    //!
    //!
    void wait_for_job_and_execute() {
        while (true) {
            std::optional < SolverJob > job = wait_for_job();

            if (job) {
                Solver solver(job.value().get_comm(),
                              solver_config, job.value().get_solver_info(), levels);

                while (solver.can_run()) {
                    solver.start(job.value());

                    int tag = send_collection_tag(job.value());

                    // Send data to Collector Group
                    solver.send(job.value(), tag,
                                scheduler.get_collector().collector_group);
                }

                job.value().send_complete(scheduler.get_comm());
            } else {
                break;
            }

        }
    }

    //! \brief If there is a job to receive,
    //!        then wait and gather information about that job.
    //!
    //!
    //!
    //! \return   An optional Job, no job is returned means that all
    //!           jobs have been dispatched.
    std::optional<SolverJob> wait_for_job() const {
        return SolverJob::wait_for_job(scheduler.get_comm());
    }

    //! \brief Sends the collection tag to the Collectors root.
    //!
    //! The Collector will receve the tag to wait for data from this ranks,
    //! alongside an identification of the job currently being collected.
    //!
    //! \param job   The job whose data shall be collected.
    //! \return      Returns the data that has been sent to the Collectors root.
    int send_collection_tag(const SolverJob &job) const {
        int tag = job.get_tag(SolverJob::CommunicationType::DataCollection, true);

        // Only the job owner sends a message
        if (job.get_comm().get_rank() == 0) {
            int buf[] = {job.get_id(), tag};
            debug::log_info(job.get_comm(),
                            "Sending collection tag '{}' to root...",
                            job.get_id()
            );
            // The message is sent to root on the master communicator
            int collection_tag = scheduler
                    .get_collector()
                    .get_tag(Collector::CommunicationType::CollectTag);
            mpiSafeCall(
                    MPI_Send(
                            buf, 2, MPI_INT, 0, collection_tag, scheduler.get_comm()
                    )
            );
        }

        return tag;
    }

private:
    //! Thread that will execute the Solver, if not this_thread.
    std::thread thr;

    //! Parent instance of the Scheduler.
    Scheduler &scheduler;
    //! Configuration parameters for the solver
    //! (shared amongs all Solver instances).
    SolverConfig solver_config;
    //! The number of levels that will be used.
    int levels = 0;
};

}