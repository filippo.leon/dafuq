/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//
// Created by filippo on 11/12/17.
//

#include "JobInfo.hpp"

namespace dafuq::base {

MPI_Datatype JobInfo::register_mpi_type() {
    debug::log_verbose(mpi::world, "Registering 'JobInfo' MPI datatype.");
    if( registered ) {
        debug::log_warning(mpi::world, "'JobInfo' MPI type is already registered!");
        return mpi_type;
    }

    // Setup JobInfo structure layout.
    int count = 1;
    int blk_length[] = {6};

    std::ptrdiff_t displacement[] = {
            offsetof(JobInfo, id),
            offsetof(JobInfo, num_ranks),
            offsetof(JobInfo, aggregate_info.level),
            offsetof(JobInfo, aggregate_info.intra_level_type),
            offsetof(JobInfo, aggregate_info.batch_size),
            offsetof(JobInfo, aggregate_info.sim_id),
    };
    MPI_Datatype types[] = { MPI_INT };

    // Create and return type
    mpiSafeCall(MPI_Type_create_struct(count, blk_length, displacement, types, &mpi_type));
    mpiSafeCall(MPI_Type_commit(&mpi_type));

    registered = true;

    return mpi_type;
}

bool JobInfo::registered = false;

MPI_Datatype JobInfo::mpi_type;

} // END namespace dafuq::base