/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 11/12/17.
//

#include "utilities/mpi.hpp"

#include "RankGroup.hpp"

namespace dafuq::base {

class RankPool {
public:
    RankPool(const mpi::Communicator & comm);

    std::vector<ContiguousRankGroup> get_free_rank_groups();

    void unassign(const ContiguousRankGroup & group);

    //! \biref Mark all ranks to be occupied by the job.
    //!
    //!
    //!
    //! \tparam Job
    //! \param group
    //! \param job
    template <class Job>
    void assign(const ContiguousRankGroup &group, Job &job) {
        assert(job.is_assigned() && "Job was not assigned!");

        for (int rank: group) {
            ranks[rank] = job.get_id();
        }
    }

    auto get_all_ranks() const {
        return ContiguousRankGroup(0, mpi::world.get_size());
    }

    int get_size() const {
        return ranks.size();
    }
private:
    std::vector<int> ranks;

    static constexpr int NO_RANK = -1;
};

}