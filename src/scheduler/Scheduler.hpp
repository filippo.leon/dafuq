/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once
//
// Created by filippo on 04/12/17.
//

#include <mpi.h>

#include <thread>
#include <optional>

#include "Executor.hpp"
#include "Collector.hpp"

#include "utilities/mpi.hpp"
#include "utilities/debug.hpp"
#include "utilities/utilities.hpp"
#include "utilities/type_traits.hpp"

#include "performance/Profiler.hpp"

#include "aggregator/BasicAggregator.hpp"

#include "Job.hpp"

#include "JobPool.hpp"
#include "RankPool.hpp"

//! \namespace dafuq
//! \brief Contains all elements of the DafUQ library.
//!
//!
//! \namespace dafuq::base
//! \brief Contains all base classes and definitions required to the functioning of a Scheduler.
//!
//!
namespace dafuq::base {

//! \brief This class in in charge of distributing jobs to all ranls, an to wait for jobs in all ranks.
//!
//!
//!
//! \tparam Solver
template<class Solver_, class Aggregator_ = BasicAggregator<typename Solver_::Aggregate>>
class Scheduler {
public:
    using Solver = Solver_;
    using SolverJob = Job<typename Solver::SolverInfo>;
    using Receiver = typename Solver::Receiver;
    using Aggregator = Aggregator_;

    using SchedulerCollector = Collector<Scheduler>;

    static_assert(std::is_same_v<decltype(
                  Solver(mpi::world, typename Solver::SolverConfig(), typename Solver::SolverInfo(), 0)
                  ), Solver>,
                  "Solver constructor must be of type 'Solver(mpi::Communicator, SolverConfig, SolverInfo, int)'"
    );

    template<class T>
    using get_num_collectors_t = decltype(T().get_num_collectors());

    Scheduler(const dafuq::mpi::Communicator &comm = mpi::world,
              int levels = 1, int ncollectors = 1)
            : Scheduler(utilities::ignore_(), comm, levels, ncollectors) {

    }

    //! \brief
    //!
    //!
    //!
    //! \param comm
    //! \param L
    template<class Tuple>
    Scheduler(const Tuple &collector_args, const dafuq::mpi::Communicator &comm = mpi::world,
              int levels = 1, int ncollectors = 1)
            : comm(comm), jobs_pool(comm), ranks_pool(comm), levels(levels), collector(*this, collector_args),
              executor(*this, levels) {

        int num_collectors = ncollectors;
        if constexpr(type_traits::is_detected<get_num_collectors_t, Receiver>::value) {
            num_collectors = collector.get_num_collectors();
        }

        if( num_collectors > comm.get_size() ) {
            debug::log_warning<debug::LogPolicy::Collective>(
                    comm,
                    "Collector size ({}) too big for number of available ranks ({}).",
                    num_collectors, comm.get_size()
            );
        }
        num_collectors = std::min(comm.get_size(), num_collectors);

        JobInfo::register_mpi_type();
        Solver::SolverInfo::register_mpi_type();

        collector.setup(num_collectors);
    }

    template <class ...Args>
    void configure(Args&&... args) {
        executor.solver_config.configure(std::forward<Args>(args)...);
    }

    void start() {
        collector.start();
        executor.start();

        schedule();
    }

    //! \brief Allocates jobs to ranks, wait for jobs to finish and assign new jobs.
    //!
    void schedule() {
        if (!is_root()) {
            return;
        }

        profiler.start_stage("schedule");

        debug::log_verbose(comm, "Rank 0 is scheduling all jobs, please wait.");

        // Loop till job queue is empty
        while (!jobs_pool.is_empty()) {
            auto gaps = ranks_pool.get_free_rank_groups();

            // Fill gaps
            for (auto &gap: gaps) {

                // Try to fit a job in the gap until gap is full or there is no suitable job
                while (gap.get_size() != 0) {
                    // Find biggest job that fits the gap
                    debug::log_verbose(comm, "Looking for job to assign, job max size is {}.", gap.get_size());

                    std::optional<int> job_id = jobs_pool.get_biggest_job_of_size(gap.get_size());

                    if (job_id.has_value()) { // found the biggest job that fits
                        debug::log_verbose(comm, "Found job to assign, job id is {}.", job_id.value());
                        // TODO: move tojob_pool

                        jobs_pool.mutex.lock();
                        auto &job = jobs_pool.get_jobs().at(job_id.value());
                        // Needed ranks for the job
                        int actual_count = job.get_min_ranks();

                        // Split off a number of ranks
                        ContiguousRankGroup ranks = gap.split(actual_count);

                        // Assign ranks to the job
                        job.assign_ranks(ranks, comm);
                        ranks_pool.assign(ranks, job);

                        jobs_pool.mutex.unlock();

                        jobs_pool.assign_job(job_id.value(), ranks);
                    } else { // no job does not fit in gap
                        break;
                    }
                }

            }

            debug::log_info(comm, "Waiting for ranks to free up.");
            wait_for_job_finish();
        }

        profiler.end_stage("schedule");
    }

    void wait() {
        profiler.start_stage("wait");

        if (is_root()) {
            debug::log_info(comm, "Done: all jobs dispatched, wait for jobs to finish...");
            while (!jobs_pool.are_all_complete()) {
                wait_for_job_finish();
            }

            debug::log_info(comm, "Done: all jobs completed, send terminate signal to all...");
            send_terminate_to_all(ranks_pool.get_all_ranks());

            debug::log_info(comm, "No more jobs to receive.");
        }

        executor.wait();
        collector.wait();

        profiler.end_stage("wait");
    }

    void add_job(SolverJob job) {
        if (!collector.is_collector()) {
            return;
        }

        if( job.get_min_ranks() > ranks_pool.get_size() ) {
            debug::log_warning(
                    comm,
                    "Not enough ranks ({}) to satisfy minimum job requirements {}, reducing job min ranks, this might cause problems.",
                    ranks_pool.get_size(), job.get_min_ranks()
            );
            job.set_min_ranks(ranks_pool.get_size());
        }

        jobs_pool.add_job(job);
    }

    void _assert_all_complete() const {
        if ( comm.is_root() ) {
            jobs_pool._assert_all_complete();
        }
    }

    const JobPool<SolverJob> & get_jobs_pool() const {
        return jobs_pool;
    }

    bool is_root() const {
        return comm.is_root();
    }

    const mpi::Communicator &get_comm() const {
        return comm;
    }

    Collector<Scheduler> &get_collector() {
        return collector;
    }

    const auto &get_profiler() const {
        return profiler;
    }
protected:
    void wait_for_job_finish() {
        std::vector<int> free_ranks = jobs_pool.wait_for_job_complete();
        ranks_pool.unassign(free_ranks);
    }

    template<class Group>
    void send_terminate_to_all(const Group &group) const {
        SolverJob job = SolverJob::termination_job();

        job.assign_ranks(group, comm);

        // Send termination to himself, s.t. we can break out of the collection loop
        int buf[] = {Collector<Scheduler>::_internal::mpi_collect_termination_id, -1};
        mpiSafeCall(MPI_Send(buf, 2, MPI_INT, 0,
                             collector.get_tag(Collector<Scheduler>::CommunicationType::CollectTag), comm));
    }

private:
    mpi::Communicator comm;

    JobPool<SolverJob> jobs_pool;

    RankPool ranks_pool;

    int levels = -1;

    Collector<Scheduler> collector;

    Executor<Scheduler> executor;

    perf::Profiler profiler = perf::Profiler("Scheduler");
};

}
