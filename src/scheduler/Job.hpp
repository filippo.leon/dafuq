/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 04/12/17.
//

#include <chrono>

#include "utilities/mpi.hpp"
#include "utilities/utilities.hpp"

#include "JobInfo.hpp"

namespace dafuq::base {

//! \brief A Job object contains all the information for a single or batched Solver run.
//!
//! A job can be assigned to a group of ranks, and provides information about how
//! the solver must run.
//!
//! \see \ref Scheduler, \ref solver, \ref user::ISolverInfo, \ref JobInfo, \ref AggregateInfo
//!
//! \tparam SolverInfo  The type for the Solver information, storing unique configuration
//!                     for the solver provided by the Job. This type must be
//!                     transferable trough MPI and provide a bunch of adapted methods.
template <class SolverInfo>
class Job {
public:
    // Make sure all internal structures are POD, this wait we can safely send them
    // trough MPI.
    static_assert(std::is_pod<SolverInfo>::value,
                  "Template argument 'SolverInfo' must be a POD type!");
    static_assert(std::is_pod<JobInfo>::value,
                  "Template argument 'JobInfo' must be a POD type!");

    //! \brief Standard constructor, creates a new generic solver without AggregateInfo.
    //!
    //! \warning The aggregate_info field will remain uninitialized.
    //!
    //! Besides storing the SolverInfo data, the world communicator, it gather two
    //! essential pieces of information:
    //!  - a unique identifier "id", unique to this Job;
    //!  - a minimal number of ranks, the minimum number of ranks this Solver *requires*.
    //!
    //! \todo Provide a way to specify preferred and max num. ranks.
    //!
    //! \param min_ranks    The minimum number of ranks this Job requires.
    //! \param solver_info  The user defined information the Solver need about this Job.
    Job(int min_ranks, const SolverInfo &solver_info = SolverInfo())
            : solver_info(solver_info), min_ranks(min_ranks) {
        info.id = counter++;
    }

    //! \brief Variant of standard constructor that also initializes AggregateInfo.
    //!
    //! In a MLMC or batched context this method must be used, as it provides extra
    //! information about the Job, such as level, IntraLevelType, etc.
    //!
    //! \param min_ranks    The minimum number of ranks this Job requires.
    //! \param aggregate_info    Provides information about the Aggregate generated by the
    //!                          solver
    //! \param solver_info  The user defined information the Solver need about this Job.
    Job(int min_ranks, const AggregateInfo &aggregate_info,
        const SolverInfo &solver_info = SolverInfo())
            : Job(min_ranks, solver_info) {
        info.aggregate_info = aggregate_info;
    }

    //! \brief Creates a new Job, that when sent to a rank
    //! indicates no more Job is available.
    //!
    //! This Job should be sent by the Scheduler to a rank, whenever no more
    //! Jobs are available to be scheduled.
    //!
    //! \return             A termination Job, to be sent to appropriate ranks.
    static Job termination_job() {
        Job j;
        j.info.id = JobInfo::termination_id;
        j.info.num_ranks = JobInfo::termination_id;
        return j;
    }

    //! \brief Send information about this job to all selected ranks in Group.
    //!
    //! Assigns the Job to a RankGroup. This sends all the necessary information
    //! to the receiving rank.
    //!
    //! The \ref Scheduler is supposed to call this method to send a Job to the
    //! \ref Executor.
    //!
    //! \see wait_for_job
    //!
    //! \tparam Group       Type of group which specifies a group of ranks.
    //! \param  ranks       The ranks to which this Job will be assigned (relative to world comm).
    //! \param  world_comm  The communicator onto which the job will execute.
    template <class Group>
    void assign_ranks(const Group &ranks, const mpi::Communicator &world_comm) {
        set_assigned();

        for(int rank: ranks) {
            assigned_ranks.push_back(rank);
        }
        info.num_ranks = ranks.get_size();

        for(int rank: assigned_ranks) {
            debug::log_verbose(world_comm, "Sending job id {} info to rank {} of {}.",
                               this->get_id(), rank, this->info.num_ranks);

            mpiSafeCall(MPI_Send(&this->info, 1, JobInfo::get(),
                                 rank,
                                 this->get_tag(CommunicationType::JobInformation),
                                 world_comm)
            );
            if(this->info.is_termination_job()) {
                debug::log_verbose(world_comm, "Sent termination job id {}"
                                           " info to rank {} of {}.",
                                   this->get_id(), rank, this->info.num_ranks);
                continue;
            }

            if constexpr ( !SolverInfo::is_void ) {
                mpiSafeCall(MPI_Send(&this->solver_info, 1,
                                     SolverInfo::get(), rank,
                                     this->get_tag(CommunicationType::SolverInformation),
                                     world_comm)
                );
            }
            mpiSafeCall(MPI_Send(assigned_ranks.data(), assigned_ranks.size(), MPI_INT,
                                 rank, this->get_tag(CommunicationType::RanksData),
                                 world_comm)
            );
        }

        assigned = true;
    }

    //! \brief Wait until a Job, or a termination Job, is received.
    //!
    //! If a Job is reeived, and it is not a termination Job, then a communicator is created,
    //! the assigned ranks are updated and the Job is marked assigned.
    //!
    //! The \ref Executor is supposed to call this method to receive a Job from the
    //! \ref Scheduler.
    //!
    //! \see assign_ranks
    //!
    //! \param world_comm   The communicator on which the Job lives.
    //! \return             A Job, or nothing if a termination Job has been received.
    static std::optional<Job> wait_for_job(const mpi::Communicator &world_comm) {
        debug::log_info(world_comm, "Waiting for job...");
        Job job;

        mpi::Status status;

        // Receive Job info
        mpiSafeCall(
                MPI_Recv(&job.info, 1, JobInfo::get(),
                         0, job.get_tag(CommunicationType::JobInformation),
                         world_comm, &status.get())
        );
        status.check();
        debug::log_info(world_comm, "Received job {}, with ranks {}...",
                        job.get_id(), job.info.num_ranks);

        // Check for termination job
        if( job.info.is_termination_job() ) {
            debug::log_verbose(world_comm, "Received termination job {}...", job.get_id());
            return std::optional < Job > ();
        }

        assert(status.tag() == job.get_tag(CommunicationType::JobInformation)
               && "Tag does not match received job!");

        if constexpr ( !SolverInfo::is_void ) {
            mpiSafeCall(
                    MPI_Recv(&job.solver_info, 1, SolverInfo::get(), 0,
                             job.get_tag(CommunicationType::SolverInformation),
                             world_comm, &status.get())
            );
            status.check();
        }

        job.assigned_ranks.resize(job.info.num_ranks);

        mpiSafeCall(
                MPI_Recv(job.assigned_ranks.data(), job.info.num_ranks,
                         MPI_INT, 0, job.get_tag(CommunicationType::RanksData), world_comm, &status.get())
        );
        status.check();

        debug::log_verbose(world_comm, "Received job {}, waiting for communicator creation, with ranks {}...",
                           job.get_id(), utilities::to_string(job.assigned_ranks));

        MPI_Group group, world_group;
        MPI_Comm_group(world_comm, &world_group);
        MPI_Group_incl(world_group, job.assigned_ranks.size(), job.assigned_ranks.data(), &group);

        MPI_Comm mpi_comm;
        mpiSafeCall(MPI_Comm_create_group(world_comm, group, 0, &mpi_comm));

        job.comm = mpi::Communicator(mpi_comm, "job-" + std::to_string(job.get_id()));

        job.set_assigned();

        debug::log_verbose(world_comm, "Communicator created, job {} successfully assigned...", job.get_id());
        return job;
    }

    //! \brief Send a message to the Scheduler that the Job is complete.
    //!
    //! The \ref Executor is supposed to call this method to send a completion message for the Job to the
    //! \ref Scheduler.
    //!
    //! \see wait_for_job_complete
    //!
    //! \param  world_comm  The communicator onto which the job will execute.
    void send_complete(const mpi::Communicator &world_comm) {
        // Only the job root sends a complete tag
        if (comm.get_rank() != 0) {
            return;
        }

        if( complete ) {
            debug::log_warning(world_comm, "Job {} already complete!", info.id);
            return;
        }
        set_complete();

        int buffer[2];
        buffer[0] = (int) JobComplete::Success;
        buffer[1] = info.id;
        int tag = get_static_tag(StaticCommunicationType::JobComplete);
        debug::log_info(world_comm, "Job {} is sending a complete message '{}´ to root, tag = {}, id = {}!",
                        info.id, buffer[0], tag, buffer[1]);
        mpiSafeCall(
                MPI_Send(buffer, 2, MPI_INT, 0,
                         tag, world_comm)
        );

    }

    //! \brief Called by the Scheduler to wait for a Job-complete message from another rank.
    //!
    //!
    //! The \ref Scheduler is supposed to call this method to receive a completion message for the Job from the
    //! \ref Executor.
    //!
    //! \see send_complete
    //!
    //! \return    Returns the id of the Job that has been complete.
    static int wait_for_job_complete(const mpi::Communicator &world_comm) {
        // We wait for a single ranks and assume the entire communicator has done its job
        mpi::Status status;

        int buf[2];
        mpiSafeCall(
                MPI_Recv(buf, 2, MPI_INT, MPI_ANY_SOURCE,
                         get_static_tag(StaticCommunicationType::JobComplete),
                         world_comm, &status.get()
                )
        );
        status.check();

        int value = buf[0];
        int id = buf[1];
        int tag = status.get().MPI_TAG;
        int sources = status.source();
        int count;
        MPI_Get_count(&status.get(), MPI_INT, &count);

        debug::log_info(world_comm,
                        "Job '{}' complete (sent from {}), success "
                                "(value = {}, tag = {}, count = {})...",
                        id, sources, value, tag, count
        );
        if (count != 2) {
            debug::log_error(world_comm,
                             "Received message has incorrect length {}.",
                             count);
            throw debug::MPIException(MPI_ERR_UNKNOWN);
        }

        return id;
    }

    //! \brief Mark a job as complete.
    //!
    //! Timestamps the start time
    void set_complete() {
        complete_time = std::chrono::steady_clock::now();
        complete = true;
    }

    //! \brief Mark a job as assigned (dispatched to an Executor).
    //!
    //! Timestamps the time
    void set_assigned() {
        assigned_time = std::chrono::steady_clock::now();
        assigned = true;
    }

    //! \brief Provides access to the ranks assigned to this Job.
    //!
    //! \return A *reference* to the assigned ranks.
    const std::vector<int> & get_assigned_ranks() const {
        return assigned_ranks;
    }

    //! \brief Specifies that the Job requires at least this number of ranks.
    //!
    //! \param new_min_ranks   The number of ranks this Job requires.
    void set_min_ranks(int new_min_ranks) {
        min_ranks = new_min_ranks;
    }

    //! \brief Obtain the number of ranks the Job requires.
    //!
    //! \return The number of ranks this Job requires.
    int get_min_ranks() const {
        return min_ranks;
    }

    //! \brief Provides access to the Solver information
    //!
    //! \return Solver information (copy).
    SolverInfo get_solver_info() const {
        return solver_info;
    }

    //! \brief Returns the unique identifier of the Job.
    //!
    //! \return Id of the Job.
    int get_id() const {
        return info.id;
    }

    //! \brief Returns the internal communicator of the Job.
    //!
    //! This returns the sub-communicator of comm, to which all ranks assigned to this Job belong.
    //!
    //! This shall not be confused with the world communicator, which is the communicator on which
    //! the Job lives.
    //!
    //! \return  Communicator consisting of the ranks to which the Job is assigned.
    const mpi::Communicator & get_comm() const {
        return comm;
    }

    //! \brief Returns whether the Job has been assigned to the Executor Group.
    //!
    //! This can be called on the Scheduler as well as on the Executor.
    //!
    //! \return  True if Job has been dispatched to ranks.
    bool is_assigned() const {
        return assigned;
    }

    //! \brief Returns true if the Job has been completed.
    //!
    //! This can be called on the Scheduler as well as on the Executor.
    //!
    //! \return True if the Job has been completed.
    bool is_complete() const {
        return complete;
    }

    //! \brief Returns a magic number representing a termination Job.
    //!
    //! This number if used to check if a Job is a termination Job, and to send information
    //! to the Executor that there is no longer a Job to process.
    //!
    //! \return A unique id belonging only to termination Jobs.
    static int termination_id() {
        return JobInfo::termination_id;
    }

    //! \brief Provides access to information about the Aggregate created by the Job.
    //!
    //!
    //!
    //! \return A copy of the aggregate information.
    AggregateInfo get_aggregate_info() const {
        return info.aggregate_info;
    }

    //! \brief Resets the counter for Job id's.
    //!
    //! This shouldn't be called for Jobs sent to the same Scheduler.
    //!
    //! \warning There is no real necessity to call this, unless you really want to have Jobs
    //!          which start with id = 0, which is needed for some test program.
    //!
    //!
    static void reset_id_counter() {
        counter = 0;
    }

    //! \brief Sets the communicator on which the Job lives.
    //!
    //! This must be done before assigning ranks.
    //!
    //! \param world_comm
    [[deprecated]] void set_world(const mpi::Communicator &world_comm) {
        this->world_comm = world_comm;
    }

private:
    //! \brief Creates an empty job.
    //!
    //! The constructor is private in order to disable creation of Jobs externally.
    //!
    //! This is used in a few cases, internally, when we need to receive a Job.
    Job() = default;

    //! Information about the Solver that is strictly dependent on the Job.
    //! \see \ref user::ISolverInfo
    SolverInfo solver_info;
    //! Information about the Job, such as the id, the number of ranks, and some \ref AggregateInfo,
    //! such as level and the \ref dafuq::base::IntraLevelType.
    JobInfo info;
    //! The minimum amount of ranks this Job requires. The default value is a Canary for
    //! wrong setup.
    int min_ranks = -1;

    // We do not send this guys via MPI
    //! Has the job been completed.
    bool complete = false;
    //! Has the job been assigned.
    bool assigned = false;

    //! Ranks to which the Job is assigned.
    std::vector<int> assigned_ranks;
    //! Intra communicator to which the Job is assigned (with ranks of assigned_ranks).
    mpi::Communicator comm;

    //! External counter for Job ids.
    static int counter;

public:
    //! Time at which the job was assigned.
    std::chrono::steady_clock::time_point assigned_time;
    //! Time at which the job was completed.
    std::chrono::steady_clock::time_point complete_time;

protected:
    //! Contains magic numbers, tags for MPI communication.
    struct _internal {
        //! The tag that discriminates the Job tags from other classes.
        static constexpr int base_tag = 400;
        //! Tag used to send SolverInfo data.
        static constexpr int mpi_solver_info_tag = base_tag + 44;
        //! Tag used to send JobInfo data.
        static constexpr int mpi_job_info_tag = base_tag + 45;
        //! Tag used to send assigned_ranks data.
        static constexpr int mpi_job_ranks_data_tag = base_tag + 46;

        //! Tag used to send complete messages.
        static constexpr int mpi_job_complete_tag = base_tag + 47;
        //! Tag used to send collection messages.
        static constexpr int mpi_job_data_collection_tag = base_tag + 56;

        //! Value send with successful job.
        static constexpr int mpi_job_complete_success_value = base_tag + 36;
    };

public:
    //! Type of communication that is being performed.
    enum class CommunicationType {
        JobInformation = _internal::mpi_job_ranks_data_tag,
        SolverInformation = _internal::mpi_job_info_tag,
        RanksData = _internal::mpi_job_ranks_data_tag,
        DataCollection = _internal::mpi_job_data_collection_tag,
    };
    //! Type of communication that is being performed, without a Job instance.
    enum class StaticCommunicationType {
        JobComplete = _internal::mpi_job_complete_tag,
    };
    //! Values sent to identity Job completion status.
    enum class JobComplete {
        Success = _internal::mpi_job_complete_success_value,
    };

    //! Get a tag for the required type of communication. Requires a Job instance.
    //!
    //! \param comm_type  The type of communication being performed.
    //! \param unique     Set to true if you require a unique tag for this communication.
    //! \return           A tag for the MPI communication.
    int get_tag(CommunicationType comm_type, bool unique = false) const {
        return (int) comm_type + (unique ? info.id : 0);
    }

    //! Get a tag for the required type of communication. Does not require a Job instance.
    //!
    //! \param comm_type  The type of communication being performed.
    //! \return           The tag for the MPI communication.
    static int get_static_tag(StaticCommunicationType comm_type) {
        return (int) comm_type;
    }
};

template <class SolverInfo>
int Job<SolverInfo>::counter = 0;

}
