/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 04/12/17.
//

#include "scheduler/RankGroup.hpp"

namespace dafuq::base {

//! \brief The Collector class provides all the required features to collect
//! the data from the finished simulations and aggregate the data to form
//! statistics estimators.
//!
//! A Collector sits into a predefined number of "collector ranks" (decided
//! at runtime), to which all the data is send, and which finalize and aggregate
//! all the data. The "collector group" might be (TODO) a dedicated group of ranks
//! or can run as thread within active ranks that also perform simulations.
//!
//! \todo Offer possibility for a dedicated group of Collectors.
//!
//! A Collector has its own MPI_Comm and communicates with the rest of the
//! rank using mpi::world.
//!
//! \see \ref scheduler_structure, \ref collection_group
//!
//! \tparam Scheduler   A collector is tied to a scheduler, from which it hinherits
//!                     the communicator and other traits.
template<class Scheduler>
class Collector {
public:
    // This is to gain access to the data withing those classes
    // as a Collector is tied to a Scheduler and an Executor.
    friend Scheduler; //!< Access to parent scheduler own members.
    friend Executor<Scheduler>;//!< Access to parent scheduler executors members.

    //! The user defined Solver class.
    using Solver = typename Scheduler::Solver;
    //! The user defined Aggregator class.
    using Aggregator = typename Scheduler::Aggregator;
    //! The user defined Receiver class.
    using Receiver = typename Scheduler::Receiver;

    //! \brief Constructor for argument-less Aggregator class.
    //!
    //! Creates a new Collector that is tied to scheduler.
    //!
    //! Default initialises the Aggregator.
    //!
    //! \param scheduler   The associated scheduler.
    Collector(Scheduler &scheduler, utilities::ignore_)
            : scheduler(scheduler), aggregator() {}

    //! \brief Constructor for forwarding arguments to the Aggregator.
    //!
    //! Creates a new Collector that is tied to scheduler.
    //!
    //! Forwards args to the Aggregator.
    //!
    //! \tparam Args        Argument types to forward to Aggregator.
    //! \param  scheduler   The associated scheduler.
    //! \param  args        The arguments fowrwarded to Aggregator.
    template<class ...Args>
    Collector(Scheduler &scheduler, Args &...args)
            : scheduler(scheduler), aggregator(args...) {}

    //! \brief Prepares the collection group.
    //!
    //! Given a number of collectors, sets up the ranks that will
    /// perform the collection of the data.
    //!
    //! Creates the communicator, group and sets up the collector group.
    //!
    //! \todo: move in constructor.
    //!
    //! \param ncollectors   The number of ranks that collect data.
    void setup(int ncollectors) {
        num_collectors = ncollectors;

        if (is_collector()) {
            debug::log_info(scheduler.get_comm(),
                            "Setting up communicator for collectors {}...",
                            utilities::to_string(get_collectors())
            );
            collectors_comm = mpi::Communicator(get_collectors(), "collector");

            int a = 0;
            int b = num_collectors;

            collector_group = ContiguousRankGroup(a, b);
        }

        // TODO: Transform this to Bcast using method of RankGroup
        int *data = nullptr;
        int size;
        std::vector<int> datav;

        if (scheduler.is_root()) {
            datav = collector_group.get_vector();
            size = datav.size();
            data = datav.data();
        }

        mpiSafeCall(MPI_Bcast(&size, 1, MPI_INT, 0, scheduler.get_comm()));

        if (!scheduler.is_root()) {
            datav.resize(size);
            data = datav.data();
        }

        mpiSafeCall(MPI_Bcast(data, size, MPI_INT, 0, scheduler.get_comm()));
        if (!scheduler.is_root()) {
            collector_group = ContiguousRankGroup(datav);
        }
    }

    //! \brief Starts the collection loop on the Collection Group.
    //!
    //! If the current rank is not in the Collection Group it returns,
    //! otherwise spawns a thread for the collection loop.
    void start() {
        if (!is_collector()) {
            return;
        } else {
            thr = std::thread(&Collector<Scheduler>::collect, this);
        }
    }

    //! \brief Wait for the collection loop to terminate.
    //!
    //!
    void wait() {
        if (thr.joinable()) {
            thr.join();
        }
    }

    //! Return true if this rank is in the Collection Group.
    bool is_collector() {
        return scheduler.get_comm().get_rank() < num_collectors;
    }

    //! Returns the rank of all collectors in the Group.
    //!
    //! The rank is relative to the scheduler world_comm global communicator.
    //!
    //! \todo In future we will return a RankGroup.
    //!
    //! \return A vector with all the ranks.
    std::vector<int> get_collectors() {
        return utilities::range(0, num_collectors);
    }

    //! \brief Returns a reference to the aggregator.
    //!
    //! Internal only: returns a reference to the aggregator
    //! of this collector.
    //!
    //! \return A reference to the aggregator.
    const Aggregator &_get_aggregator() const {
        return aggregator;
    }

protected:
    //! \brief Start the collection loop.
    //!
    //! \warning This enters an infinite loop unless
    //! a collection id is sent with
    //! termination information.
    //!
    //! This should start in its own thread.
    void collect() {
        // TODO/FIXME: might need to send job info to collectors
        debug::log_info(collectors_comm,
                        "Collector is starting the collection loop..."
        );

        while (true) {
            // TODO: collect
            try {
                auto[can_continue, tag, id] = wait_for_collection_tag();

                if (!can_continue) {
                    break;
                }

                scheduler.get_jobs_pool().mutex.lock();
                std::pair<typename Aggregator::KeyType,
                        typename Solver::Aggregate> key_val_pair;

                auto &job = scheduler.get_jobs_pool().get_jobs().at(id);
                key_val_pair = receiver.receive(job, tag, collectors_comm);

                auto ainfo = job.get_aggregate_info();
                scheduler.get_jobs_pool().mutex.unlock();

                aggregator.add(key_val_pair, ainfo);
            } catch (...) {
                continue;
            }
        }
        debug::log_info<debug::LogPolicy::Collective>(collectors_comm,
                                                      "All data has been collected."
        );
        aggregator.finalize();

        aggregator.write();
        // TODO: collect and combine data
    }

    //! \brief All the ranks in the collector wait to obain
    //! the information about the data that it is about to receive.
    //!
    //! The root rank in the collector receives the tag/info from
    //! any rank in the base communicator, then broadcasts
    //! the tag to all other ranks in the collector_communicator.
    //! All other ranks (in the collector communicator) wait
    //! to obtain the tag from the root.
    //! After this, all ranks in the collector group are ready
    //! to receive the data from the same tag.
    //!
    //! \return A tuple containing a flag indicating if id indicates collect
    //!         termination, the tag, and the id of the job to collect.
    std::tuple<bool, int, int> wait_for_collection_tag() {

        debug::log_info<debug::LogPolicy::Collective>(
                collectors_comm,
                "Collector is awaiting for data from any rank..."
        );
        int buf[2];
        // Await a new collection message at root
        if (collectors_comm.is_root()) {
            mpi::Status status;
            mpiSafeCall(MPI_Recv(buf, 2, MPI_INT, MPI_ANY_SOURCE,
                                 get_tag(CommunicationType::CollectTag),
                                 scheduler.get_comm(), &status.get()));
            status.check();

            int count;
            MPI_Get_count(&status.get(), MPI_INT, &count);
            if (count != 2) {
                debug::log_error(scheduler.get_comm(),
                                 "Received message has incorrect length {}, "
                                         "while waiting for collection tag.",
                                 count);
                throw debug::MPIException(MPI_ERR_UNKNOWN);
            }
        }

        // Broadcast collection message to all collectors
        mpiSafeCall(MPI_Bcast(buf, 2, MPI_INT, 0, collectors_comm));

        int id = buf[0];
        int tag = buf[1];
        if (id == _internal::mpi_collect_termination_id) {
            debug::log_info(collectors_comm,
                            "Collector is terminating data collection (id = {}).",
                            id
            );
        } else {
            debug::log_info(collectors_comm,
                            "Collector awaiting for data from job id '{}'.", id);
        }

        return {id != _internal::mpi_collect_termination_id, tag, id};
    }

private:
    //! A reference to the parent scheduler.
    Scheduler &scheduler;

    //! The thread that will be used to run the collection loop.
    std::thread thr;

    //! Contains the group of collector ranks,
    //! available to all ranks (not only in Collector Group)
    //! after setup_collectors()
    ContiguousRankGroup collector_group;

    //! A receiver class that takes care of collecting the data from executors.
    Receiver receiver;
    //! The aggregator class that takes care of "gluing" togheter the data.
    Aggregator aggregator;

    //! The communicator for intra-Collector Group communication.
    //! Valid only if is_collector() is true.
    mpi::Communicator collectors_comm;

    //! The number of ranks in collectors_comm;
    int num_collectors = 1;

    //! Internal strucutre holding tags for MPI communication.
    struct _internal {
        //! A base tag for disctriminating with other classes.
        static constexpr int base_tag = 500;
        //! The MPI tag which is sent when collection is performed.
        static constexpr int mpi_collect_tag = base_tag + 55;
        //! The job id sent when the collection is terminated.
        static constexpr int mpi_collect_termination_id = -5;
    };

    //! An enum specifying the type of communication that is performed.
    //!
    enum class CommunicationType {
        CollectTag = _internal::mpi_collect_tag,
    };

    //! \brief Returns the correct tag for the specified CommunicationType
    //!
    //! Returns a unique tag for the requested communication.
    //!
    //! \return A MPI tag to be used for this type of communication.
    int get_tag(CommunicationType enumerator) const {
        return (int) enumerator;
    }
};

}