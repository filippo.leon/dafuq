[TOC]

DafUQ (Dafuq: an Awesome Framework for Uncertainty Quantification),
is a library which provides essential routines for massively parallel
uncertainty quantification in HPC.

The documentation can be found at the Gitlab
[Pages](http://filippo.leon.gitlab.io/dafuq/).

# Compilation # {#compilation}

DafUQ uses CMake as `Makefile` generator. A very simple compilation
example is:

~~~
mkdir build && cd build
cmake .. -D<options>
make -j<n>
~~~

If the documentation is enable, it can be compiled with `make doc`.

## CMake options ## {#cmake_options}

| Option name | Values | Description  |
| ------------------- | --------- | --- |
| DOXYGEN_ENABLE      |  ON      | Enable this documentation. |
| SANITIZE            |  ON      | Enable compiler sanitizers. |

# Extra # ${#extra}

