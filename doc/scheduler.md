Scheduler Structure {#scheduler_structure}
===================

[TOC]

# Basic structure # {#basic_structure}

A Scheduler is an object that is in charge of distributing jobs, running then
and collecting the final outputs
from each job. Each scheduler must be created collectively on all ranks
of the MPI communicator.

A Scheduler performs three operations:
- Distribution (Scheduling),
- Execution, and
- Collection.

Each of these operations is delegated to a specific class, namely:
- execution is dispatched to \ref dafuq::base::Executor "Executor";
- collection is dispatched to \ref dafuq::base::Collector "Collector";
- scheduling is handled by \ref dafuq::base::Scheduler "Scheduler" itself.

Any rank might perform any of these three operations asyncronously, and we will
refer to this ranks as belonging to the "Scheduling Group", "Execution Group",
and "Collection Group".

## Scheduler Group ## {#scheduler_group}

TODO

## Collection Group ## {#collection_group}

A rank in the Collector Group is in charge of receiving data of all finalized
simulation and construct an \ref aggregator from the
received \ref aggregate.

Currently a Collector lives in the first `num_collectors` ranks
of the communicator,
and spawns a thread s.t. collection, executions and scheduling
can be performed concurrently by the same process.

The collector, once setted up and within the `collect()` loop, will wait
for a 'collection tag' from an \ref dafuq::base::Executor
(alongside with an identification
of the executor itself). Once this is received, it will forward the tag to
the \ref Receiver, s.t. the data can be collected. Once the data has
been sent from the Executor to the Receiver, the Collector will
collect and process the data.

## Execution Group ## {#execution_group}

TODO

## Aggregator ## {#aggregator}

\ref dafuq::base::BasicAggregator
\ref dafuq::base::MLMCAggregator

TODO

# User data # {#user_data}

## Solver ## {#solver}

A Solver is the main component of the Simulation. This class provides a way to process the Job received.
A Solver class executes on the \ref Executor Group and sends the data to the \ref Collector Group via
an instance of a \ref receiver.

A reference implementation can be found in \ref dafuq::user::ISolver.

## Aggregate ## {#aggregate}

An aggregate represent a collection of solutions of a \ref solver, which is received by the \ref Collector
through an implementation of a \ref receiver.

In order to provide a meaningful agggregate, you should implement a suitable class, which follows the prototype
\ref dafuq::user::IAggregate (i.e. it must have some basic operator).

Some fundamental types (float, double, etc.) are valid aggregates. Using integers as aggregates is specifically 
disabled in order to prevent bugs, as the result should never be integer.

## Receiver ## {#receiver}

A receiver is in charge to collect data sent from the \ref Execution Group, whenever a \ref Job has finished
one stage and is ready to send the data. The receiving function is called within the \ref Collector Group.

The `receive` function in the Receiver is directly ties to the `send` function withing a \ref Solver
instance (\see dafuq::user::ISolver).

A reference implementation can be found in \ref dafuq::user::IReceiver.

## Interpolator (upscaling) ## {#interpolator}

An Interpolator provides a Functor for the interpolation from Lower to Higher levels in a multi-level context.

A reference implementation can be found in \ref dafuq::user::IInterpolator.