/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//
// Created by filippo on 11/12/17.
//

#include <random>

#include "../test.hpp"

#include "user/IInterpolator.hpp"

#include "aggregator/MLMCAggregator.hpp"

using namespace dafuq::base;
using namespace dafuq::user;

BOOST_AUTO_TEST_CASE(basic_aggregator, *boost::unit_test::tolerance(0.00001)) {
    BasicAggregator<double> aggr;

    AggregateInfo info;

    int N = 10;
    for (int i = 1; i <= N; ++i) {
        aggr.add({0, i}, info);
    }

    aggr.finalize();

    BOOST_TEST(aggr.get(0).mean == (N + 1) / 2.);
    BOOST_TEST(aggr.get(0).m2 == (N > 1 ? (double(N) * N - 1) / 12 * N / (N - 1) : 0.));
    BOOST_TEST(aggr.get(0).count == N);
}

BOOST_AUTO_TEST_CASE(mlmc_aggregator, *boost::unit_test::tolerance(0.00001)) {
    IInterpolator<double> interp;
    MLMCAggregator<IInterpolator<double>, double> aggr(interp);

    AggregateInfo info;
    info.level = 0;
    info.intra_level_type = IntraLevelType::High;

    int N = 10;
    for (int i = 1; i <= N; ++i) {
        aggr.add({0, i}, info);
    }

    const auto &accumulator = aggr.finalize();

    BOOST_TEST(accumulator.at(0).mean == (N + 1) / 2.);
    BOOST_TEST(accumulator.at(0).m2 == (N > 1 ? (double(N) * N - 1) / 12 * N / (N - 1) : 0.));
    BOOST_TEST(accumulator.at(0).count == N);
}

auto linear_poly_aggregator(int nlevels, int ncoarse = 1) {
    IInterpolator<double> interp;
    MLMCAggregator<IInterpolator<double>, double> aggr(interp);

    std::vector<int> nnodes = utilities::range(1, nlevels);
    std::transform(nnodes.begin(), nnodes.end(), nnodes.begin(), [ncoarse](int i) { return (1 << i) * ncoarse; });

    int fevals = 0;

    int keys = 3;
    constexpr int DEG = 3;

    const std::vector<std::array<double, DEG + 1>> coefficients = {
            {1, 0, 0, 0},
            {0, 1, 0, 0},
            {1, 0, 1, 1}
    };

    // TODO: generic polynomial, dependence on k and parameters
    auto f = [](const std::array<double, DEG + 1> &coeff, double node) {
        return coeff[0] + node * (coeff[1] + node * (coeff[2] + coeff[3] * node));
    };
    // Uniform grid

    double a = 0;
    double b = 1;

    auto get_nodes = [a, b](int size) {
        auto range = utilities::range(0, size);
        std::vector<double> ret(range.size());
        std::transform(range.begin(), range.end(), ret.begin(),
                       [a, b, size](int i) {
                           return double(i + 1 / 2.) / (size) * (b - a);
                       }
        );
        return ret;
    };

    auto mean = [a, b, coefficients = coefficients](int k) {
        double ret = 0;
        for (int i = DEG; i >= 0; --i) {
            ret += coefficients[k][i] * (std::pow(b, i + 1) - std::pow(a, i + 1)) / (i + 1);
        }
        return ret;
    };
    auto var = [a, b, coefficients = coefficients, mean](int k) {
        auto &c = coefficients[k];
        auto m = mean(k);

        return +1. * (b - a) * c[0] * c[0]
               + 1. * (b * b - a * a) * c[0] * c[1]
               + 1. / 3. * (b * b * b - a * a * a) * c[1] * c[1]
               + 2. / 3. * (b * b * b - a * a * a) * c[0] * c[2]
               + 1. / 2. * (b * b * b * b - a * a * a * a) * c[1] * c[2]
               + 1. / 2. * (b * b * b * b - a * a * a * a) * c[0] * c[3]
               + 1. / 5. * (b * b * b * b * b - a * a * a * a * a) * c[2] * c[2]
               + 2. / 5. * (b * b * b * b * b - a * a * a * a * a) * c[1] * c[3]
               + 1. / 3. * (b * b * b * b * b * b - a * a * a * a * a * a) * c[2] * c[3]
               + 1. / 7. * (b * b * b * b * b * b * b - a * a * a * a * a * a * a) * c[3] * c[3] - m * m;
    };

    std::vector<double> nodes, old_nodes;

    for (int l = 0; l < nlevels; ++l) {
        AggregateInfo info;
        info.level = l;
        info.intra_level_type = IntraLevelType::High;

        old_nodes = std::move(nodes);
        nodes = get_nodes(nnodes[l]);

        for (double n: nodes) {
            for (int k = 0; k < keys; ++k) {
                double r = f(coefficients[k], n);
                fevals++;
                aggr.add({k, r}, info);
            }
        }


        if (l == 0) { continue; }
        info.intra_level_type = IntraLevelType::Low;

        for (double n: old_nodes) {
            for (int k = 0; k < keys; ++k) {
                double r = f(coefficients[k], n);
                fevals++;
                aggr.add({k, r}, info);
            }
        }

    }

    return std::make_tuple(aggr.finalize(), mean, var, fevals, keys);
}

BOOST_AUTO_TEST_CASE(mlmc_linear_poly_aggregator, *boost::unit_test::tolerance(0.001)) {

    int nlevels = 10;
    [[maybe_unused]] auto[accumulator, mean, var, _, keys] = linear_poly_aggregator(nlevels);

    for (int k = 0; k < keys; ++k) {
        BOOST_TEST(accumulator.at(k).mean == mean(k));
        BOOST_TEST(accumulator.at(k).m2 == var(k));
        BOOST_TEST(accumulator.at(k).count == 2);
    }

    std::stringstream ss[3], ss_mc[3];
    std::string header = fmt::format(
            "{:5} | {:10} {:10} {:10} | {:10} {:10} {:10} | {:7}\n",
            "L", "mean", "ex-mean", "err", "var", "ex-var", "err", "evals"
    );
    for (int I = 0; I < 3; ++I) {
        ss[I] << header;
        ss_mc[I] << header;
    }
    for (int i = 1; i <= 10; ++i) {
        auto[accumulator, mean, var, feval, keys] = linear_poly_aggregator(i);
        for (int k = 0; k < keys; ++k) {
            ss[k] << fmt::format(
                    "{:5} | {:10.4f} {:10.4f} {:10.4f} | {:10.4f} {:10.4f} {:10.4f} | {:7}\n",
                    i,
                    accumulator.at(k).mean, mean(k), std::abs(accumulator.at(k).mean - mean(k)),
                    accumulator.at(k).m2, var(k), std::abs(accumulator.at(k).m2 - var(k)),
                    feval
            );
        }
        [[maybe_unused]] auto[mcaccumulator, mcmean, mcvar, _, mckeys] = linear_poly_aggregator(1, feval);
        for (int k = 0; k < keys; ++k) {
            ss_mc[k] << fmt::format(
                    "{:5} | {:10.4f} {:10.4f} {:10.4f} | {:10.4f} {:10.4f} {:10.4f} | {:7}\n",
                    i,
                    mcaccumulator.at(k).mean, mcmean(k), std::abs(mcaccumulator.at(k).mean - mcmean(k)),
                    mcaccumulator.at(k).m2, mcvar(k), std::abs(mcaccumulator.at(k).m2 - mcvar(k)),
                    feval
            );
        }
    }
    for (int I = 0; I < 3; ++I) {
        std::cout << std::flush << ss[I].str() << std::flush;
        std::cout << std::flush << ss_mc[I].str() << std::flush;
    }
}

BOOST_AUTO_TEST_CASE(mlmc_random_aggregator, *boost::unit_test::tolerance(0.2)) {
    double low = 0., high = 1.;

    std::default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());
    std::uniform_real_distribution<double> distribution(low, high);

    double mean = 0.5;
    double var = (high - low) * (high - low) / 12.;

    IInterpolator<double> interp;
    MLMCAggregator<IInterpolator<double>, double> aggr(interp);

    int keys = 3;
    int nlevels = 10;
    int N = 1000;

    for (int l = 0; l < nlevels; ++l) {
        AggregateInfo info;
        info.level = l;
        info.intra_level_type = IntraLevelType::High;

        for (int i = 1; i <= N; ++i) {
            for (int k = 0; k < keys; ++k) {
                double r = distribution(generator);
                aggr.add({k, r}, info);
            }
        }

        // TODO: add improvement for this
        if (l == 0) { continue; }
        info.intra_level_type = IntraLevelType::Low;

        for (int i = 1; i <= N; ++i) {
            for (int k = 0; k < keys; ++k) {
                double r = distribution(generator);
                aggr.add({k, r}, info);
            }
        }
    }

    const auto &accumulator = aggr.finalize();

    for (int k = 0; k < keys; ++k) {
        BOOST_TEST_WARN(accumulator.at(k).mean == mean, boost::test_tools::tolerance(std::sqrt(var)));
        BOOST_TEST_WARN(accumulator.at(k).m2 == var);
        BOOST_TEST(accumulator.at(k).count == N);
    }
}

// TODO: test compute area of circle

TEST_MAIN()