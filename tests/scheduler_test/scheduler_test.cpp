/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//
// Created by filippo on 11/12/17.
//

#include <thread>

#include "../test.hpp"

#include "user/ISolverInfo.hpp"

using namespace dafuq;
using namespace dafuq::base;

using VoidSolverInfo = user::ISolverInfo<>;

void wait_for_job() {
    std::optional<Job<VoidSolverInfo>> job_real = Job<VoidSolverInfo>::wait_for_job(mpi::world);
    BOOST_CHECK(job_real.has_value());
    BOOST_CHECK(job_real.value().get_id() == 0);

    std::optional<Job<VoidSolverInfo>> job_real1 = Job<VoidSolverInfo>::wait_for_job(mpi::world);
    BOOST_CHECK(job_real1.has_value());
    BOOST_CHECK(job_real1.value().get_id() == 1);

    std::optional<Job<VoidSolverInfo>> job_end = Job<VoidSolverInfo>::wait_for_job(mpi::world);
    BOOST_CHECK(!job_end.has_value());
}

void wait_for_job_and_send_complete() {
    std::optional<Job<VoidSolverInfo>> job_real = Job<VoidSolverInfo>::wait_for_job(mpi::world);
    BOOST_CHECK(job_real.has_value());
    BOOST_CHECK(job_real.value().get_id() == 2);

    job_real.value().send_complete(mpi::world);

    std::optional<Job<VoidSolverInfo>> job_real1 = Job<VoidSolverInfo>::wait_for_job(mpi::world);
    BOOST_CHECK(job_real1.has_value());
    BOOST_CHECK(job_real1.value().get_id() == 3);

    job_real1.value().send_complete(mpi::world);

    std::optional<Job<VoidSolverInfo>> job_end = Job<VoidSolverInfo>::wait_for_job(mpi::world);
    BOOST_CHECK(!job_end.has_value());
}

BOOST_AUTO_TEST_CASE(simple_scheduler) {

    JobInfo::register_mpi_type();
    VoidSolverInfo::register_mpi_type();

    std::thread thr(wait_for_job);

    if (mpi::world.get_rank() == 0) {
        ContiguousRankGroup grp(0, mpi::world.get_size());

        Job<VoidSolverInfo> job(mpi::world.get_size());
        // Assign ranks to the job
        job.assign_ranks(grp, mpi::world);
        Job<VoidSolverInfo> job1(mpi::world.get_size());
        // Assign ranks to the job
        job1.assign_ranks(grp, mpi::world);

        Job<VoidSolverInfo> job_end = Job<VoidSolverInfo>::termination_job();

        job_end.assign_ranks(grp, mpi::world);
    }

    thr.join();

}

BOOST_AUTO_TEST_CASE(simple_scheduler2) {

    JobInfo::register_mpi_type();
    VoidSolverInfo::register_mpi_type();

    std::thread thr(wait_for_job_and_send_complete);

    if (mpi::world.get_rank() == 0) {
        ContiguousRankGroup grp(0, mpi::world.get_size());

        Job<VoidSolverInfo> job(mpi::world.get_size());
        // Assign ranks to the job
        job.assign_ranks(grp, mpi::world);

        int id1;
        try {
            id1 = Job<VoidSolverInfo>::wait_for_job_complete(mpi::world);
        } catch (debug::MPIException) {
            std::cout << "MPI Exception occurred furing wait_for_job_complete, trying to recover.\n";
        }
        BOOST_CHECK(id1 == 2);

        Job<VoidSolverInfo> job1(mpi::world.get_size());
        // Assign ranks to the job
        job1.assign_ranks(grp, mpi::world);

        int id2 = Job<VoidSolverInfo>::wait_for_job_complete(mpi::world);
        BOOST_CHECK(id2 == 3);

        Job<VoidSolverInfo> job_end = Job<VoidSolverInfo>::termination_job();

        job_end.assign_ranks(grp, mpi::world);
    }

    thr.join();
}

TEST_MAIN()
