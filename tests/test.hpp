/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 11/12/17.
//

#define BOOST_TEST_MODULE test
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>

#include "scheduler/Scheduler.hpp"

#include "io/options.hpp"

using namespace dafuq;

#define TEST_MAIN() \
int main(int argc, char* argv[]) { \
    mpi::init(&argc, &argv); \
    int ret = boost::unit_test::unit_test_main(init_unit_test, argc, argv); \
    mpi::finalize(); \
    return ret; \
}
