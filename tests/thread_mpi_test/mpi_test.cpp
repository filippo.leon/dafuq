/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//
// Created by filippo on 11/12/17.
//

#include "../test.hpp"

#include "sampler/sampler.hpp"

#include "utilities/mpi_operators.hpp"

using namespace dafuq;

BOOST_AUTO_TEST_CASE(mpi_test) {

    int val = 5;

    mpi::bcast(val, 0);

    BOOST_CHECK(val == 5);

    std::vector<int> ran = utilities::range(0, 9);
    std::vector<int> ran_recv = utilities::range(0, 9);

    mpi::bcast(ran_recv, 0);

    if (!mpi::world.is_root()) {
        BOOST_CHECK(utilities::are_equal(ran, ran_recv));
    }

}

TEST_MAIN()