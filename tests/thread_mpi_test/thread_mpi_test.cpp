/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//
// Created by filippo on 11/12/17.
//

#include <thread>

#include "boost/test/minimal.hpp"

#include "utilities/mpi.hpp"

constexpr int tag = 48;

using namespace dafuq;

void thread() {
    MPI_Status status;
    int buf[2];
    mpiSafeCall(MPI_Recv(buf, 2, MPI_INT, MPI_ANY_SOURCE, tag, mpi::world, &status));

    BOOST_CHECK(buf[0] == 4);
    BOOST_CHECK(buf[1] == 48);
    BOOST_CHECK(status.MPI_TAG == tag);
    BOOST_CHECK(status.MPI_SOURCE == 0);
    int count;
    MPI_Get_count(&status, MPI_INT, &count);
    BOOST_CHECK(count == 2);
}

int test_main(int argc, char **argv) {
    mpi::init(&argc, &argv);

    if (mpi::world.get_rank() == 0) {
        std::thread thr(thread);

        int val[] = {4, 48};
        mpiSafeCall(MPI_Send(val, 2, MPI_INT, 0, tag, mpi::world));

        thr.join();
    }

    return mpi::finalize();
}