/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//
// Created by filippo on 11/12/17.
//

#include "../test.hpp"

using namespace dafuq;
using namespace dafuq::base;

BOOST_AUTO_TEST_CASE(simple_job_info) {

    JobInfo::register_mpi_type();

    int tag = 44;

    mpi::Request req;

    JobInfo info, info_recv;

    if(mpi::world.is_root() ) {
        mpiSafeCall(MPI_Isend(&info, 1, JobInfo::get(),
                              mpi::world.get_size() - 1, tag, mpi::world, &req.get())
        );
    }

    if( mpi::world.get_rank() == mpi::world.get_size() - 1 ) {
        mpi::Status status;
        mpiSafeCall(MPI_Recv(&info_recv, 1, JobInfo::get(),
                             0, tag, mpi::world, &status.get())
        );
        status.check();

        BOOST_CHECK( info.id == info_recv.id );
        BOOST_CHECK( info.num_ranks == info_recv.num_ranks );
    }

    if(mpi::world.is_root() ) {
        req.wait();
    }
}

BOOST_AUTO_TEST_CASE(extra_job_info) {

    JobInfo::register_mpi_type();

    int tag = 49;

    mpi::Request req;

    JobInfo info, info_recv;

    std::srand((unsigned int) std::chrono::system_clock::now().time_since_epoch().count());
    info.id = std::rand();
    info.num_ranks = std::rand();
    info.aggregate_info.level = std::rand();

    IntraLevelType types[] = {IntraLevelType::None, IntraLevelType::Low, IntraLevelType::High, IntraLevelType::All,};
    info.aggregate_info.intra_level_type = types[std::rand() % 3];

    if (mpi::world.is_root()) {
        mpiSafeCall(MPI_Isend(&info, 1, JobInfo::get(),
                              mpi::world.get_size() - 1, tag, mpi::world, &req.get())
        );
    }

    if (mpi::world.get_rank() == mpi::world.get_size() - 1) {
        mpi::Status status;
        mpiSafeCall(MPI_Recv(&info_recv, 1, JobInfo::get(),
                             0, tag, mpi::world, &status.get())
        );
        status.check();

        BOOST_CHECK(info.id == info_recv.id);
        BOOST_CHECK(info.num_ranks == info_recv.num_ranks);
        BOOST_CHECK(info.aggregate_info.level == info_recv.aggregate_info.level);
        BOOST_CHECK(info.aggregate_info.intra_level_type == info_recv.aggregate_info.intra_level_type);
    }

    if (mpi::world.is_root()) {
        req.wait();
    }
}


TEST_MAIN()