/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 12/12/17.
//

#include "user/ISolver.hpp"
#include "user/ISolverInfo.hpp"
#include "user/ISolverConfig.hpp"
#include "user/IReceiver.hpp"

#include "scheduler/Job.hpp"

using namespace dafuq;

class BasicSolverConfig : public user::ISolverConfig {
public:
    void configure(float wt) {
        wait_time = wt;
    }

    float wait_time;
};

class BasicSolverReceiver : public user::IReceiver<double> {
public:
    int get_collector_size() {
        return 1;
    }

    template<class Job>
    std::pair<int, double> receive(const Job &job, int tag, const mpi::Communicator &collector_comm) {
        // In this case, no matter how big the collector is, we just collect from the root
        if (!collector_comm.is_root()) {
            return {0, 0.};
        }

        // We keep track of the number of times receive is called
        int id = job.get_id();
        int key = counts[id]++;

        mpi::Status status;
        int value;

        int rank = job.get_assigned_ranks().at(0);
        mpiSafeCall(MPI_Recv(&value, 1, MPI_INT, rank, tag, mpi::world, &status.get()));
        status.check();
        debug::log_info(mpi::world, "Receiving data '{}' from rank {}.", value, rank);

        return {key, value};
    }

private:
    std::map<int, int> counts;
};

class BasicSolver : public user::ISolver<user::ISolverInfo<>, BasicSolverConfig, double, BasicSolverReceiver> {
public:
    BasicSolver(const mpi::Communicator &/*comm*/,
                const SolverConfig &config, const SolverInfo &/*info*/, int /*levels*/)
            : config(config) {

    }

    virtual ~BasicSolver() = default;

    /*Solution*/void start(const base::Job<SolverInfo> &/*job*/) {
        debug::log_info(mpi::world, "Solver started, wait time = {}", config.wait_time);

        count++;

        std::this_thread::sleep_for(std::chrono::milliseconds((int) (config.wait_time * 1000.)));
    }

    bool can_run() const {
        return count < nstarts;
    }

    template<class Group>
    void send(const base::Job<SolverInfo> &job, int tag,
              const Group & /*collector_group*/) {
        int value_to_send = 100 * job.get_id() + count;

        if (job.get_comm().is_root()) {
            debug::log_info(mpi::world, "Sending data '{}' to {}.", value_to_send, 0);
            mpiSafeCall(MPI_Send(&value_to_send, 1, MPI_INT, 0, tag, mpi::world));
        }
    }

    int count = 0;
    const int nstarts = 2;

    const SolverConfig &config;
};
