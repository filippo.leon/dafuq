/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//
// Created by filippo on 11/12/17.
//

#include "../test.hpp"

#include "basic_solver.hpp"
#include "io/serialization.hpp"

BOOST_AUTO_TEST_CASE(simple_scheduler, *boost::unit_test::tolerance(0.00001)) {
    base::Scheduler<BasicSolver> scheduler(mpi::world, 1, 2);

    scheduler.configure(1);

    scheduler.add_job(BasicSolver::Job(1));
    scheduler.add_job(BasicSolver::Job(1));
    scheduler.add_job(BasicSolver::Job(1));
    scheduler.add_job(BasicSolver::Job(1));
    scheduler.add_job(BasicSolver::Job(2));
    scheduler.add_job(BasicSolver::Job(2));
    scheduler.add_job(BasicSolver::Job(2));
    scheduler.add_job(BasicSolver::Job(2));
    scheduler.add_job(BasicSolver::Job(2));

    scheduler.start();
    scheduler.wait();

    scheduler._assert_all_complete();

    auto &map = scheduler.get_collector()._get_aggregator()._get_map();
    if (mpi::world.get_rank() == 0) {
        BOOST_CHECK( map.size() == 2 );

        int I = 0;
        for(auto & elem: map) {
            BOOST_TEST(elem.second.mean == 401. + I++);
            BOOST_TEST(elem.second.m2 == 75000.);
            BOOST_CHECK( elem.second.count == 9 );
        }

        auto tree = boost::property_tree::ptree{};
        serialization::serialize_job_pool(tree, scheduler.get_jobs_pool());
        serialization::write_xml(tree, "jobs.xml");
    } else if (scheduler.get_collector().is_collector()) {
        BOOST_CHECK( map.size() == 1 );

        for(auto & elem: map) {
            BOOST_TEST(elem.second.mean == 0. );
            BOOST_TEST(elem.second.m2 == 0. );
            BOOST_CHECK( elem.second.count == 18 );
        }
    } else {
        BOOST_CHECK( map.size() == 0 );
    }
}

BOOST_AUTO_TEST_CASE(start_stop_scheduler, *boost::unit_test::tolerance(0.00001)) {
    base::Scheduler<BasicSolver> scheduler(mpi::world, 1, 2);

    scheduler.configure(1);

    scheduler.add_job(BasicSolver::Job(1));
    scheduler.add_job(BasicSolver::Job(1));
    scheduler.add_job(BasicSolver::Job(1));
    scheduler.add_job(BasicSolver::Job(1));
    scheduler.add_job(BasicSolver::Job(2));

    scheduler.start();

    scheduler.add_job(BasicSolver::Job(2));
    scheduler.add_job(BasicSolver::Job(2));
    scheduler.add_job(BasicSolver::Job(2));
    scheduler.add_job(BasicSolver::Job(2));

    scheduler.schedule();
    scheduler.wait();

    scheduler._assert_all_complete();

    auto &map = scheduler.get_collector()._get_aggregator()._get_map();
    if (mpi::world.get_rank() == 0) {
        BOOST_CHECK( map.size() == 2 );

        int I = 0;
        for(auto & elem: map) {
            BOOST_TEST(elem.second.mean == 401. + I++);
            BOOST_TEST(elem.second.m2 == 75000.);
            debug::log_info(mpi::world, "m2 difference = {}", elem.second.m2 - 75000.);
            BOOST_CHECK( elem.second.count == 9 );
        }

        auto tree = boost::property_tree::ptree{};
        serialization::serialize_job_pool(tree, scheduler.get_jobs_pool());
        serialization::write_xml(tree, "jobs.xml");
    } else if (scheduler.get_collector().is_collector()) {
        BOOST_CHECK( map.size() == 1 );

        for(auto & elem: map) {
            BOOST_TEST(elem.second.mean == 0. );
            BOOST_TEST(elem.second.m2 == 0. );
            BOOST_CHECK( elem.second.count == 18 );
        }
    } else {
        BOOST_CHECK( map.size() == 0 );
    }
}

TEST_MAIN()
