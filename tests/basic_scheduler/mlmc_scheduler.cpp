/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//
// Created by filippo on 11/12/17.
//

#include "../test.hpp"

#include "basic_solver.hpp"
#include "io/serialization.hpp"

#include "user/IInterpolator.hpp"

#include "aggregator/MLMCAggregator.hpp"

using namespace dafuq::base;

using Aggregator = base::MLMCAggregator<user::IInterpolator<double>, double>;

BOOST_AUTO_TEST_CASE(mlmc_scheduler, *boost::unit_test::tolerance(0.001)) {
    auto collargs = std::make_tuple(user::IInterpolator<double>());

    base::Scheduler<BasicSolver, Aggregator>
            scheduler(collargs, mpi::world, 1, 2);

    scheduler.configure(0.01);

    int nlevels = 5;
    int batch_size = 1;
    int sim_id = 0;
    std::vector<int> nsims{1, 2, 4, 8, 16};

    for (int l = 0; l < nlevels; ++l) {
        for (int s = 0; s < nsims[l]; ++s) {
            // TODO aggregate sends batch
            // TODO handle diff, and batches
            // TODO handle id same for diff and for batches
            AggregateInfo info_high = {l, IntraLevelType::High, batch_size, sim_id};

            scheduler.add_job(BasicSolver::Job(1, info_high));

            if (l > 0) {
                AggregateInfo info_low = {l, IntraLevelType::Low, batch_size, sim_id};

                scheduler.add_job(BasicSolver::Job(1, info_low));
            }

            sim_id++;
        }
    }

    scheduler.start();

    scheduler.wait();

    scheduler._assert_all_complete();


    if (mpi::world.get_rank() == 0) {
        auto tree = serialization::serialize_scheduler(scheduler);
        serialization::write_xml(tree, "jobs.xml");
    }
}

TEST_MAIN()
