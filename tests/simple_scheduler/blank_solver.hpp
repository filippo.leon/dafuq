/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//
// Created by filippo on 12/12/17.
//

#include "user/ISolver.hpp"
#include "user/ISolverInfo.hpp"
#include "user/ISolverConfig.hpp"

#include "scheduler/Job.hpp"

using namespace dafuq;

class BlankSolver : public user::ISolver<> {
public:
    BlankSolver(const mpi::Communicator &/*comm*/,
                const SolverConfig & /*config*/,
                const SolverInfo & /*info*/, int /*levels*/) {

    }

    virtual ~BlankSolver() = default;

    /*Solution*/void start(const base::Job<SolverInfo> & /*job*/) {
        debug::log_info(mpi::world, "Solver started.");

        count++;
    }

    bool can_run() const {
        return count < nstarts;
    }

    template<class Group>
    void send(const base::Job<SolverInfo> &/*job*/, int /*tag*/, const Group &/*group*/) {

    }

    int count = 0;
    const int nstarts = 1;
};
