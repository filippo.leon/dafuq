/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//
// Created by filippo on 11/12/17.
//

#include "../test.hpp"

#include "blank_solver.hpp"

BOOST_AUTO_TEST_CASE( simple_scheduler ) {
    base::Scheduler<BlankSolver> scheduler(mpi::world, 1, 2);

    scheduler.add_job(BlankSolver::Job(1));
    scheduler.add_job(BlankSolver::Job(1));
    scheduler.add_job(BlankSolver::Job(1));
    scheduler.add_job(BlankSolver::Job(1));
    scheduler.add_job(BlankSolver::Job(1));
    scheduler.add_job(BlankSolver::Job(1));
    scheduler.add_job(BlankSolver::Job(1));
    scheduler.add_job(BlankSolver::Job(1));
    scheduler.add_job(BlankSolver::Job(1));
    scheduler.add_job(BlankSolver::Job(1));
    scheduler.add_job(BlankSolver::Job(1));
    scheduler.add_job(BlankSolver::Job(2));

    scheduler.start();
    scheduler.wait();

    scheduler._assert_all_complete();
}

BOOST_AUTO_TEST_CASE(simple_dup_scheduler) {
    mpi::Communicator comm = mpi::world.duplicate();

    base::Scheduler<BlankSolver> scheduler(comm, 1, 2);

    scheduler.add_job(BlankSolver::Job(1));
    scheduler.add_job(BlankSolver::Job(1));
    scheduler.add_job(BlankSolver::Job(2));
    scheduler.add_job(BlankSolver::Job(1));
    scheduler.add_job(BlankSolver::Job(2));
    scheduler.add_job(BlankSolver::Job(3));
    scheduler.add_job(BlankSolver::Job(2));

    scheduler.start();
    scheduler.wait();

    scheduler._assert_all_complete();
}

BOOST_AUTO_TEST_CASE(simple_smaller_scheduler) {
    mpi::Communicator comm = mpi::world.split(mpi::world.get_rank() < 5 ? 0 : MPI_UNDEFINED, 0);

    if (mpi::world.get_rank() >= 5) return;

    base::Scheduler<BlankSolver> scheduler(comm, 1, 2);

    scheduler.add_job(BlankSolver::Job(5));
    scheduler.add_job(BlankSolver::Job(1));
    scheduler.add_job(BlankSolver::Job(3));
    scheduler.add_job(BlankSolver::Job(2));
    scheduler.add_job(BlankSolver::Job(2));

    scheduler.start();
    scheduler.wait();

    scheduler._assert_all_complete();
}

TEST_MAIN()
