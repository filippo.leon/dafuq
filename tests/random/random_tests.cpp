/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//
// Created by filippo on 11/12/17.
//

#include "../test.hpp"

#include "sampler/sampler.hpp"

#include "utilities/mpi_operators.hpp"

BOOST_AUTO_TEST_CASE(sampler_test) {

    dafuq::random::Sampler<> sampler;
    dafuq::random::Sampler<> sampler2;

    dafuq::random::Sampler<> sampler_a(44);
    dafuq::random::Sampler<> sampler_b(44);

    sampler.configure<0>(0, 1);
    sampler2.configure<0>(0, 1);
    sampler_a.configure<0>(0, 1);
    sampler_b.configure<0>(0, 1);

    sampler.set_index(5);
    sampler2.set_index(9);

    sampler_a.set_index(11);
    sampler_b.set_index(11);

    double a = sampler.get<0>(5);
    double b = sampler.get<0>(4);
    double c = sampler.get<0>(5);
    double d = sampler.get<0>(4);

    double a_2 = sampler2.get<0>(5);
    double b_2 = sampler2.get<0>(4);

    BOOST_CHECK(a == c);
    BOOST_CHECK(b == d);

    BOOST_CHECK(a != a_2);
    BOOST_CHECK(b != b_2);

    double a_a = sampler_a.get<0>(14);
    double b_a = sampler_a.get<0>(9);
    double b_b = sampler_b.get<0>(9);
    double a_b = sampler_b.get<0>(14);

    BOOST_CHECK(a_a == a_b);
    BOOST_CHECK(b_a == b_b);

    sampler_a.set_index(16);
    sampler_a.set_index(11);

    a_a = sampler_a.get<0>(14);
    b_a = sampler_a.get<0>(9);

    BOOST_CHECK(a_a == a_b);
    BOOST_CHECK(b_a == b_b);
}

BOOST_AUTO_TEST_CASE(sampler_mpi_test) {

    dafuq::random::Sampler<> sampler;

    sampler.set_index(44);

    double a = sampler.get<0>(5);

    mpi::bcast(a, 0);
}

TEST_MAIN()
